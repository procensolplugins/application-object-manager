/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.appiancorp.suiteapi.content.ContentConstants;
import com.appiancorp.suiteapi.content.exceptions.InvalidContentException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.bean.SearchFilterBean;

// TODO: Auto-generated Javadoc
/**
 * The Class Utils.
 */
public class Utils {

	/**
	 * Convert to json.
	 *
	 * @param list the list
	 * @return the string[]
	 */
	public static String[] convertToJson(List list) {

		Gson gson = new GsonBuilder().create();

		String[] jsonArr = new String[list.size()];
		int index = 0;
		for (Object obj : list) {
			jsonArr[index++] = gson.toJson(obj);
		}
		return jsonArr;
	}

	/**
	 * Validate pagination.
	 *
	 * @param startIndex the start index
	 * @param batchSize  the batch size
	 * @return the string
	 */
	public static ResultBean validatePagination(int startIndex, int batchSize) {

		StringBuffer sb = new StringBuffer();
		sb.append((startIndex <= 0) ? "Start index must be 1 or greater. " : ConstantIfc.EMPTY_STR);
		sb.append((batchSize > 0 && batchSize <= ConstantIfc.BATCH_SIZE) ? ConstantIfc.EMPTY_STR
				: "Batchsize must be greater than or equal 1 and less than " + ConstantIfc.BATCH_SIZE + ".");
		String validationErrorTxt = sb.toString();

		if (validationErrorTxt != null && !ConstantIfc.EMPTY_STR.equalsIgnoreCase(validationErrorTxt)) {
			return new ResultBean(true, validationErrorTxt);
		} else {
			return null;
		}

	}

	/**
	 * Convert to json.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String convertToJson(String str) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(str);
	}

	/**
	 * Convert to json.
	 *
	 * @param map the map
	 * @return the string
	 */
	public static String convertToJson(Map map) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(map);
	}

	/**
	 * Convert to json.
	 *
	 * @param obj the obj
	 * @return the string
	 */
	public static String convertToJson(Object obj) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(obj);
	}

	/**
	 * Gets the result map json.
	 *
	 * @param errorTxt the error txt
	 * @param data     the data
	 * @return the result map json
	 */
	public static String getResultMapJson(String errorTxt, List data) {
		Map resultMap = new HashMap();
		if (errorTxt != null && !ConstantIfc.EMPTY_STR.equalsIgnoreCase(errorTxt)) {
			resultMap.put(ConstantIfc.ERROR_JSON_LBL, errorTxt);
			resultMap.put(ConstantIfc.DATA_JSON_LBL, null);
			return Utils.convertToJson(resultMap);
		}
		resultMap.put(ConstantIfc.ERROR_JSON_LBL, null);
		resultMap.put(ConstantIfc.DATA_JSON_LBL, data);
		return Utils.convertToJson(resultMap);
	}

	/**
	 * Removes the duplicates.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static List removeDuplicates(List list) {
		return new ArrayList(new LinkedHashSet<>(list));
	}

	/**
	 * Compare date.
	 *
	 * @param anchorDate the anchor date
	 * @param campareTo the campare to
	 * @param operation the operation
	 * @return true, if successful
	 * @throws ParseException the parse exception
	 */
	public static boolean compareDate(Date anchorDate, String[] campareTo, String operation) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date[] dates = new Date[campareTo.length];

		for (int index = 0; index < campareTo.length; index++) {
			dates[index] = sdf.parse(campareTo[index]);
		}

		return compareDate(anchorDate, dates, operation);
	}

	/**
	 * Compare date.
	 *
	 * @param anchorDate the anchor date
	 * @param campareTo the campare to
	 * @param operation the operation
	 * @return true, if successful
	 */
	public static boolean compareDate(Date anchorDate, Date[] campareTo, String operation) {

		Calendar anchorCal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		Calendar cal3 = Calendar.getInstance();
		anchorCal.setTime(anchorDate);
		cal2.setTime(campareTo[0]);

		if (ConstantIfc.OP_BEFORE.equalsIgnoreCase(operation)) {
			return anchorCal.before(cal2);
		} else if (ConstantIfc.OP_AFTER.equalsIgnoreCase(operation)) {
			return anchorCal.after(cal2);
		} else if (ConstantIfc.OP_IS.equalsIgnoreCase(operation)) {
			return anchorCal.equals(cal2);
		} else if (ConstantIfc.OP_BETWEEN.equalsIgnoreCase(operation)) {
			cal3.setTime(campareTo[1]);
			return ((anchorCal.equals(cal2) || anchorCal.after(cal2))
					&& (anchorCal.equals(cal3) || anchorCal.before(cal3)));
		} else {
			return false;
		}
	}

	/**
	 * Checks if is null or blank.
	 *
	 * @param str the str
	 * @param trimIt the trim it
	 * @return true, if is null or blank
	 */
	public static boolean isNullOrBlank(String str, boolean trimIt) {
		if (trimIt) {
			return !(str != null && !"".equalsIgnoreCase(str.trim()));
		}
		return !(str != null && !"".equalsIgnoreCase(str));
	}

	/**
	 * Checks if is null or blank.
	 *
	 * @param str the str
	 * @return true, if is null or blank
	 */
	public static boolean isNullOrBlank(String str) {
		return isNullOrBlank(str, true);
	}

	/**
	 * Sanitize.
	 *
	 * @param string the string
	 * @param regex the regex
	 * @param replacement the replacement
	 * @return the string
	 */
	public static String sanitize(String string, String regex, String replacement) {
		return isNullOrBlank(string) ? string : string.replaceAll(regex, replacement);
	}

	/**
	 * Checks if is valid search filter field.
	 *
	 * @param field the field
	 * @return true, if is valid search filter field
	 */
	public static boolean isValidSearchFilterField(String field) {
		return Utils.isNullOrBlank(field) ? false
				: (SearchFilterBean.FIELD_NAME.equalsIgnoreCase(field)
						|| SearchFilterBean.OPERATION.equalsIgnoreCase(field)
						|| SearchFilterBean.OPRANDS.equalsIgnoreCase(field));

	}

	/**
	 * Checks if is valid search filter.
	 *
	 * @param searchFilterArr the search filter arr
	 * @return true, if is valid search filter
	 */
	public static boolean isValidSearchFilter(String[] searchFilterArr) {

		for (String filter : searchFilterArr) {
			if (!isValidSearchFilter(filter)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if is valid search filter.
	 *
	 * @param searchFilterTxt the search filter txt
	 * @return true, if is valid search filter
	 */
	public static boolean isValidSearchFilter(String searchFilterTxt) {

		if (Utils.isNullOrBlank(searchFilterTxt)) {
			return false;
		}
		String keyValuesArr[] = searchFilterTxt.split(";");
		if (keyValuesArr.length != 3) {
			return false;
		}
		Set<String> processedKeySet = new LinkedHashSet<>();
		for (String keyValue : keyValuesArr) {
			if (Utils.isNullOrBlank(keyValue)) {
				return false;
			}
			String[] keyValueArr = keyValue.split(":");
			if (keyValueArr.length != 2) {
				return false;
			}
			if (Utils.isNullOrBlank(keyValueArr[0]) || Utils.isNullOrBlank(keyValueArr[1])) {
				return false;
			}
			if (!isValidSearchFilterField(keyValueArr[0])) {
				return false;
			}
			processedKeySet.add(keyValueArr[0]);
		}
		if (processedKeySet.size() != 3) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the content object type.
	 *
	 * @param type the type
	 * @param subType the sub type
	 * @return the content object type
	 */
	public static String getContentObjectType(Integer type, Integer subType) {

		type = type == null ? 0 : type;
		subType = subType == null ? 0 : subType;

		switch (type) {

		case ContentConstants.TYPE_ALL:
			return ConstantIfc.TYPE_ALL;

		case ContentConstants.TYPE_ANY_KC:
			return ConstantIfc.TYPE_ANY_KC;

		case ContentConstants.TYPE_APPLICATION:
			return ConstantIfc.TYPE_APPLICATION;

		case ContentConstants.TYPE_COMMUNITY:
			return ConstantIfc.TYPE_COMMUNITY;

		case ContentConstants.TYPE_COMMUNITY_KC:
			return ConstantIfc.TYPE_COMMUNITY_KC;

		case ContentConstants.TYPE_CONTAINER:
			return ConstantIfc.TYPE_CONTAINER;

		case ContentConstants.TYPE_CONTENT:
			return ConstantIfc.TYPE_CONTENT;

		case ContentConstants.TYPE_CUSTOM:
			return ConstantIfc.TYPE_CUSTOM;

		case ContentConstants.TYPE_DOCUMENT:
			return ConstantIfc.TYPE_DOCUMENT;

		case ContentConstants.TYPE_FOLDER:

			switch (subType) {
			case ContentConstants.SUBTYPE_FOLDER_APPLICATIONS:
				return ConstantIfc.SUBTYPE_FOLDER_APPLICATIONS;

			case ContentConstants.SUBTYPE_FOLDER_DATA_STORE_CONFIGS:
				return ConstantIfc.SUBTYPE_FOLDER_DATA_STORE_CONFIGS;

			case ContentConstants.SUBTYPE_FOLDER_KNOWLEDGE:
				return ConstantIfc.SUBTYPE_FOLDER_KNOWLEDGE;

			case ContentConstants.SUBTYPE_FOLDER_MIGRATION_FLAGS:
				return ConstantIfc.SUBTYPE_FOLDER_MIGRATION_FLAGS;

			case ContentConstants.SUBTYPE_FOLDER_RULES:
				return ConstantIfc.SUBTYPE_FOLDER_RULES;

			default:
				return ConstantIfc.TYPE_FOLDER.concat(ConstantIfc.BLANK_STR).concat(ConstantIfc.TYPE_IGNORE);
			}

		case ContentConstants.TYPE_NON_CONTAINER:
			return ConstantIfc.TYPE_NON_CONTAINER;

		case ContentConstants.TYPE_NON_DOCUMENT:
			return ConstantIfc.TYPE_NON_DOCUMENT;

		case ContentConstants.TYPE_PERSONAL_KC:
			return ConstantIfc.TYPE_PERSONAL_KC;

		case ContentConstants.TYPE_RULE:
			switch (subType) {

			case ContentConstants.SUBTYPE_RULE_CONSTANT:
				return ConstantIfc.SUBTYPE_RULE_CONSTANT;

			case ContentConstants.SUBTYPE_RULE_DECISION:
				return ConstantIfc.SUBTYPE_RULE_DECISION;

			case ContentConstants.SUBTYPE_RULE_FREEFORM:
				return ConstantIfc.SUBTYPE_RULE_FREEFORM;

			case ContentConstants.SUBTYPE_RULE_INTERFACE:
				return ConstantIfc.SUBTYPE_RULE_INTERFACE;

			case ContentConstants.SUBTYPE_RULE_OUTBOUND_INTEGRATION:
				return ConstantIfc.SUBTYPE_RULE_OUTBOUND_INTEGRATION;

			case ContentConstants.SUBTYPE_RULE_QUERY:
				return ConstantIfc.SUBTYPE_RULE_QUERY;

			default:
				return ConstantIfc.TYPE_RULE.concat(ConstantIfc.BLANK_STR).concat(ConstantIfc.TYPE_IGNORE);
			}

		}
		return ConstantIfc.TYPE_IGNORE;

	}

	/**
	 * Gets the internal file path.
	 *
	 * @param internalFileNameWithPath the internal file name with path
	 * @return the internal file path
	 */
	public static String getInternalFilePath(String internalFileNameWithPath) {
		try {

			if (internalFileNameWithPath == null || internalFileNameWithPath.isEmpty()) {
				return ConstantIfc.NA;
			}
			return internalFileNameWithPath.substring(internalFileNameWithPath.indexOf(ConstantIfc.ACCDOCS));
		} catch (Exception e) {
			return ConstantIfc.NA;
		}

	}

	/**
	 * Format model location.
	 *
	 * @param modelLocations the model locations
	 * @param parentName the parent name
	 * @return the string
	 */
	public static String formatModelLocation(String[] modelLocations, String parentName) {
		String modelLocation = Arrays.toString(modelLocations);
		if (modelLocation.equals("[]")) {
			return new String(parentName);
		}
		return new String(modelLocation.substring(1, modelLocation.length() - 1).replace(",", "/") + "/" + parentName);
	}

	/**
	 * Format string with local.
	 *
	 * @param inputStr the input str
	 * @return the string
	 */
	public static String formatStringWithLocal(String inputStr) {
		
		String[] names = inputStr.replaceAll("\\{", "").replaceAll("\\}", "").split(",");
		
		Map<String, String> namesMap= new HashMap<String, String>();
		for (String name : names) {
			String []lang = name.split("=");
			namesMap.put(lang[0].trim(), lang.length > 1 ?  lang[1] : "NA");
		}
		return Utils.convertToJson(namesMap);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		SearchFilterBean s1 = new SearchFilterBean("TEST", "Equal to", new String[] { "TEST1", "TEST2" });

		System.out.println(Utils.isValidSearchFilter("fieldName:taskName;operation:=;oprands:Script task"));
		s1.buildMe("fieldName:TEST;operation:Equal to;oprands:2019/06/23, 2019/06/23");
		System.out.println(s1);
		System.out.println(getContentObjectType(1, null));
	}

}
