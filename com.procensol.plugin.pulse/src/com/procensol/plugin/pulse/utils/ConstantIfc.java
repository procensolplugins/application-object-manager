/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.utils;

// TODO: Auto-generated Javadoc
/**
 * The Interface ConstantIfc.
 */
public interface ConstantIfc {

	/** The Constant ERR_TXT_PREFIX. */
	public static final String ERR_TXT_PREFIX = "Error while performing ";

	/** The Constant VALIDATION_ERR_TXT_PREFIX. */
	public static final String VALIDATION_ERR_TXT_PREFIX = "Validation error while performing ";

	/** The Constant HYPHEN. */
	public static final String NA = "NA";

	/** The Constant HYPHEN. */
	public static final String HYPHEN = " - ";

	/** The Constant DECIMAL. */
	public static final String DECIMAL = ".";

	/** The Constant COMMA. */
	public static final String COMMA = ", ";

	/** The Constant NEW_LINE. */
	public static final String NEW_LINE = "\n";

	/** The Constant TAB. */
	public static final String TAB = "\t";

	/** The Constant TRUE_STR. */
	public static final String TRUE_STR = "True";

	/** The Constant FALSE_STR. */
	public static final String FALSE_STR = "False";

	/** **** Report Column Names ************. */
	public static final Object REPORT_COLUMN_PROCESSMODELID = "id";

	/** The Constant REPORT_COLUMN_PROCESSMODELNAME. */
	public static final Object REPORT_COLUMN_PROCESSMODELNAME = "c0";

	/** The Constant REPORT_COLUMN_PROCESSMODELDESC. */
	public static final Object REPORT_COLUMN_PROCESSMODELDESC = "c1";

	/** The Constant REPORT_COLUMN_PROCESSMODELCREATEDBY. */
	public static final Object REPORT_COLUMN_PROCESSMODELCREATEDBY = "c2";

	/** The Constant REPORT_COLUMN_PROCESSMODELCREATEDON. */
	public static final Object REPORT_COLUMN_PROCESSMODELCREATEDON = "c3";

	/** The Constant REPORT_COLUMN_PROCESSMODELVERSION. */
	public static final Object REPORT_COLUMN_PROCESSMODELVERSION = "version";

	/** The Constant BATCH_SIZE. */
	public static final Integer BATCH_SIZE = 50;

	/** The Constant BLANK. */
	public static final String EMPTY_STR = "";
	
	/** The Constant BLANK_STR. */
	public static final String BLANK_STR = " ";

	/** The Constant ERROR_JSON_LBL. */
	public static final String ERROR_JSON_LBL = "error";

	/** The Constant DATA_JSON_LBL. */
	public static final String DATA_JSON_LBL = "data";

	/** The Constant TOTALCOUNT_JSON_LBL. */
	public static final String TOTALCOUNT_JSON_LBL = "totalCount";

	/** ****************************************. */

	public static final String ADMINISTERED_PROPERTY_KEY = "adminSetting";
	
	/** The Constant DATATYPE_KEY. */
	public static final String DATATYPE_KEY = "datatype";
	
	/** The Constant DATA_STORE_KEY. */
	public static final String DATA_STORE_KEY = "dataStore";
	
	/** The Constant CONTENT_KEY. */
	public static final String CONTENT_KEY = "content";
	
	/** The Constant GROUP_KEY. */
	public static final String GROUP_KEY = "group";
	
	/** The Constant GROUP_TYPE_KEY. */
	public static final String GROUP_TYPE_KEY = "groupType";
	
	/** The Constant FORUM_KEY. */
	public static final String FORUM_KEY = "forum";
	
	/** The Constant USER_KEY. */
	public static final String USER_KEY = "user";
	
	/** The Constant PROCESS_MODEL_FOLDER_KEY. */
	public static final String PROCESS_MODEL_FOLDER_KEY = "processModelFolder";
	
	/** The Constant PROCESS_MODEL_KEY. */
	public static final String PROCESS_MODEL_KEY = "processModel";
	
	/** The Constant PORTLET_KEY. */
	public static final String PORTLET_KEY = "portlet";
	
	/** The Constant PAGE_KEY. */
	public static final String PAGE_KEY = "page";
	
	/** The Constant APPLICATION_KEY. */
	public static final String APPLICATION_KEY = "application";
	
	/** The Constant TEMPO_FEED_KEY. */
	public static final String TEMPO_FEED_KEY = "tempoFeed";
	
	/** The Constant RECORD_TYPE_KEY. */
	public static final String RECORD_TYPE_KEY = "recordType";
	
	/** The Constant TEMPO_REPORT_KEY. */
	public static final String TEMPO_REPORT_KEY = "tempoReport";
	
	/** The Constant TASK_REPORT_KEY. */
	public static final String TASK_REPORT_KEY = "taskReport";
	
	/** The Constant WEB_API_KEY. */
	public static final String WEB_API_KEY = "webApi";
	
	/** The Constant SITE_KEY. */
	public static final String SITE_KEY = "site";
	
	/** The Constant THIRD_PARTY_CREDENTIALS_KEY. */
	public static final String THIRD_PARTY_CREDENTIALS_KEY = "thirdPartyCredentials";
	
	/** The Constant DATA_SOURCE_KEY. */
	public static final String DATA_SOURCE_KEY = "dataSource";
	
	/** The Constant EMBEDDED_SAIL_THEME_KEY. */
	public static final String EMBEDDED_SAIL_THEME_KEY = "embeddedSailTheme";
	
	/** The Constant CONNECTED_SYSTEM_KEY. */
	public static final String CONNECTED_SYSTEM_KEY = "connectedSystem";

	
	/** The Constant APPIAN_OBJECT_KEYS. */
	public static final String[] APPIAN_OBJECT_KEYS = new String[] { ADMINISTERED_PROPERTY_KEY, DATATYPE_KEY,
			DATA_STORE_KEY, CONTENT_KEY, GROUP_KEY, GROUP_TYPE_KEY, FORUM_KEY, USER_KEY, PROCESS_MODEL_FOLDER_KEY,
			PROCESS_MODEL_KEY, PORTLET_KEY, PAGE_KEY, APPLICATION_KEY, TEMPO_FEED_KEY, RECORD_TYPE_KEY,
			TEMPO_REPORT_KEY, TASK_REPORT_KEY, WEB_API_KEY, SITE_KEY, THIRD_PARTY_CREDENTIALS_KEY, DATA_SOURCE_KEY,
			EMBEDDED_SAIL_THEME_KEY, CONNECTED_SYSTEM_KEY

	};

	/** The Constant TYPE_DOCUMENT. */
	public static final String TYPE_DOCUMENT ="document";
	
	/** The Constant TYPE_FOLDER. */
	public static final String TYPE_FOLDER ="folder";
	
	/** The Constant TYPE_PERSONAL_KC. */
	public static final String TYPE_PERSONAL_KC="personalKc";
	
	/** The Constant TYPE_COMMUNITY_KC. */
	public static final String TYPE_COMMUNITY_KC ="communityKc";
	
	/** The Constant TYPE_COMMUNITY. */
	public static final String TYPE_COMMUNITY="community";
	
	/** The Constant TYPE_RULE. */
	public static final String TYPE_RULE ="rule";
	
	/** The Constant TYPE_APPLICATION. */
	public static final String TYPE_APPLICATION="application";
	
	/** The Constant TYPE_CUSTOM. */
	public static final String TYPE_CUSTOM ="custom";
	
	/** The Constant TYPE_ANY_KC. */
	public static final String TYPE_ANY_KC ="anyKc";
	
	/** The Constant TYPE_CONTAINER. */
	public static final String TYPE_CONTAINER="container";
	
	/** The Constant TYPE_IGNORE. */
	public static final String TYPE_IGNORE ="ignore";
	
	/** The Constant TYPE_ALL. */
	public static final String TYPE_ALL="all";
	
	/** The Constant TYPE_NON_CONTAINER. */
	public static final String TYPE_NON_CONTAINER="nonContainer";
	
	/** The Constant TYPE_NON_DOCUMENT. */
	public static final String TYPE_NON_DOCUMENT ="nonDocument";
	
	/** The Constant TYPE_CONTENT. */
	public static final String TYPE_CONTENT ="content";
	
	/** The Constant SUBTYPE_RULE_CONSTANT. */
	public static final String SUBTYPE_RULE_CONSTANT ="ruleConstant";
	
	/** The Constant SUBTYPE_RULE_FREEFORM. */
	public static final String SUBTYPE_RULE_FREEFORM ="ruleFreeform";
	
	/** The Constant SUBTYPE_RULE_QUERY. */
	public static final String SUBTYPE_RULE_QUERY="ruleQuery";
	
	/** The Constant SUBTYPE_RULE_DECISION. */
	public static final String SUBTYPE_RULE_DECISION ="ruleDecision";
	
	/** The Constant SUBTYPE_RULE_OUTBOUND_INTEGRATION. */
	public static final String SUBTYPE_RULE_OUTBOUND_INTEGRATION = "ruleOutboundIntegration";
	
	/** The Constant SUBTYPE_RULE_INTERFACE. */
	public static final String SUBTYPE_RULE_INTERFACE = "ruleInterface";
	
	/** The Constant SUBTYPE_FOLDER_KNOWLEDGE. */
	public static final String SUBTYPE_FOLDER_KNOWLEDGE="folderKnowledge";
	
	/** The Constant SUBTYPE_FOLDER_RULES. */
	public static final String SUBTYPE_FOLDER_RULES="folderRules";
	
	/** The Constant SUBTYPE_FOLDER_APPLICATIONS. */
	public static final String SUBTYPE_FOLDER_APPLICATIONS ="folderApplications";
	
	/** The Constant SUBTYPE_FOLDER_DATA_STORE_CONFIGS. */
	public static final String SUBTYPE_FOLDER_DATA_STORE_CONFIGS ="folderDataStoreConfigs";
	
	/** The Constant SUBTYPE_FOLDER_MIGRATION_FLAGS. */
	public static final String SUBTYPE_FOLDER_MIGRATION_FLAGS="folderMigrationFlags";
	
	/** The Constant SUBTYPE_DATA_STORE_CONFIG. */
	public static final String SUBTYPE_DATA_STORE_CONFIG ="dataStoreConfig";
	
	/** The Constant SUBTYPE_CONNECTED_SYSTEM. */
	public static final String SUBTYPE_CONNECTED_SYSTEM="connectedSystem";
	
	/** The Constant SUBTYPE_MIGRATION_FLAG. */
	public static final String SUBTYPE_MIGRATION_FLAG="migrationFlag";
	
	/** The Constant OP_IS. */
	public static final String OP_IS = "IS";
	
	/** The Constant OP_LIKE. */
	public static final String OP_LIKE = "LIKE";
	
	/** The Constant OP_AFTER. */
	public static final String OP_AFTER = "AFTER";
	
	/** The Constant OP_BEFORE. */
	public static final String OP_BEFORE = "BEFORE";
	
	/** The Constant OP_BETWEEN. */
	public static final String OP_BETWEEN = "BETWEEN";
	
	/** The Constant MIN_START_INDEX. */
	public static final int MIN_START_INDEX = -1;
	
	/** The Constant MAX_BATCH_SIZE. */
	public static final int MAX_BATCH_SIZE = -1;
	
	/** The Constant CURRENT_VERSION_OFFSET. */
	public static final int CURRENT_VERSION_OFFSET = 1;

/** The Constant ACCDOCS. */
public static final String ACCDOCS = "accdocs";

}
