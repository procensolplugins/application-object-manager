/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.manager;

import java.util.List;

import org.apache.log4j.Logger;

import com.appiancorp.suiteapi.applications.Application;
import com.appiancorp.suiteapi.applications.ApplicationNotFoundException;
import com.appiancorp.suiteapi.applications.ApplicationService;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.process.ProcessDesignService;
import com.appiancorp.suiteapi.type.TypeService;
import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.bean.AppianObjectsCollisionBean;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.dao.impl.ApplicationDao;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessManager.
 */
public class ApplicationManager {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ApplicationManager.class);

	/** The manager. */
	private static ApplicationManager manager = null;

	/** The pe service. */
	ApplicationService appService = null;
	
	/** The contenet service. */
	ContentService contenetService = null;
	
	/** The pd service. */
	ProcessDesignService pdService = null;
	
	/** The type service. */
	TypeService typeService = null;

	/**
	 * Instantiates a new application manager.
	 *
	 * @param appService the app service
	 * @param contentService the content service
	 * @param pdService the pd service
	 * @param typeService the type service
	 */
	private ApplicationManager(ApplicationService appService, ContentService contentService,
			ProcessDesignService pdService, TypeService typeService) {
		this.appService = appService;
		this.contenetService = contentService;
		this.pdService = pdService;
		this.typeService = typeService;
	}

	/**
	 * Gets the manager.
	 *
	 * @param appService the app service
	 * @param contentService the content service
	 * @param pdService the pd service
	 * @param typeService the type service
	 * @return the manager
	 */
	public static ApplicationManager getManager(ApplicationService appService, ContentService contentService,
			ProcessDesignService pdService, TypeService typeService) {
		if (manager == null) {
			manager = new ApplicationManager(appService, contentService, pdService, typeService);
		}
		return manager;
	}

	/**
	 * Gets the process alerts.
	 *
	 * @param startIndex the start index
	 * @param batchSize  the batch size
	 * @param includeDraft the include draft
	 * @return the process alerts
	 */
	public String getAllApplications(int startIndex, int batchSize, boolean includeDraft) {
		try {
			ResultBean rsb = null;
			rsb = Utils.validatePagination(startIndex, batchSize);

			if (rsb != null) {
				return Utils.convertToJson(rsb);
			}
			ApplicationDao applicationDao = ApplicationDao.getDao(appService, contenetService, pdService, typeService);
			ResultBean rs = applicationDao.getAllAppliacations(startIndex, batchSize, includeDraft);
			return Utils.convertToJson(rs);

		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.debug(ex);
			return Utils.convertToJson(new ResultBean(true, ex.getMessage()));
		}
	}
	
	

	/**
	 * Gets the application objects.
	 *
	 * @param applicationUuidOrId the application uuid or id
	 * @return the application objects
	 */
	public String getApplicationObjects(String applicationUuidOrId) {
		ResultBean rsb = new ResultBean(true, "Unknown Error");
		try {
			
			ApplicationDao applicationDao = ApplicationDao.getDao(appService, contenetService, pdService, typeService);

			return Utils.convertToJson(applicationDao.getApplicationObjects(applicationUuidOrId));

		} catch (PrivilegeException | ApplicationNotFoundException e) {
			e.printStackTrace();
			LOG.debug(e);
			rsb = new ResultBean(true, e.getMessage());
		}

		return Utils.convertToJson(rsb);
	}

	/**
	 * Gets the application.
	 *
	 * @param uuidOrId the uuid or id
	 * @return the application
	 */
	public String getApplication(String uuidOrId) {
		ResultBean rsb = new ResultBean(true, "Unknown Error");
		try {
			ApplicationDao applicationDao = ApplicationDao.getDao(appService, contenetService, pdService, typeService);
			rsb = applicationDao.getApplication(uuidOrId);

		} catch (PrivilegeException | ApplicationNotFoundException e) {
			e.printStackTrace();
			rsb = new ResultBean(true, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			rsb = new ResultBean(true, e.getMessage());
		}

		return Utils.convertToJson(rsb);

	}

	/**
	 * Gets the collisions.
	 *
	 * @param applicationUUIDsOrId the application UUI ds or id
	 * @return the collisions
	 */
	public String getCollisions(String[] applicationUUIDsOrId) {
		ResultBean rsb = new ResultBean(true, "Unknown Error");
		try {
			ApplicationDao applicationDao = ApplicationDao.getDao(appService, contenetService, pdService, typeService);

			List<AppianObjectsCollisionBean> appObjCollisionMap = applicationDao
					.getCollidedObjectsList(applicationUUIDsOrId);

			rsb = new ResultBean(appObjCollisionMap.size(), appObjCollisionMap);

		} catch (PrivilegeException | ApplicationNotFoundException e) {
			e.printStackTrace();
			rsb = new ResultBean(true, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			rsb = new ResultBean(true, e.getMessage());
		}

		return Utils.convertToJson(rsb);
	}

}