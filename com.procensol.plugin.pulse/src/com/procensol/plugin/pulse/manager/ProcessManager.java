/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.manager;

import org.apache.log4j.Logger;

import com.appiancorp.suiteapi.common.exceptions.ExpressionException;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.content.exceptions.InvalidContentException;
import com.appiancorp.suiteapi.process.ProcessExecutionService;
import com.appiancorp.suiteapi.process.analytics2.ProcessAnalyticsService;
import com.appiancorp.suiteapi.process.exceptions.ReportComplexityException;
import com.appiancorp.suiteapi.process.exceptions.ReportSizeException;
import com.appiancorp.suiteapi.process.exceptions.UnsupportedReportSpecificationException;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.dao.impl.ProcessDao;
import com.procensol.plugin.pulse.dao.impl.ReportDao;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessManager.
 */
public class ProcessManager {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ProcessManager.class);

	/** The manager. */
	private static ProcessManager manager = null;

	/** The pe service. */
	ProcessExecutionService peService = null;

	/** The pa service. */
	ProcessAnalyticsService paService = null;

	/** The content service. */
	ContentService contentService = null;

	/**
	 * Instantiates a new process manager.
	 *
	 * @param peService
	 *            the pe service
	 * @param paService
	 *            the pa service
	 * @param contentService
	 *            the content service
	 */
	ProcessManager(ProcessExecutionService peService, ProcessAnalyticsService paService,
			ContentService contentService) {
		this.peService = peService;
		this.paService = paService;
		this.contentService = contentService;
	}

	/**
	 * Gets the manager.
	 *
	 * @param peService
	 *            the pe service
	 * @param paService
	 *            the pa service
	 * @param contentService
	 *            the content service
	 * @return the manager
	 */
	public static ProcessManager getManager(ProcessExecutionService peService, ProcessAnalyticsService paService,
			ContentService contentService) {
		if (manager == null) {
			manager = new ProcessManager(peService, paService, contentService);
		}
		return manager;
	}

	/**
	 * Gets the process alerts.
	 *
	 * @param startIndex
	 *            the start index
	 * @param batchSize
	 *            the batch size
	 * @return the process alerts
	 */
	public String getProcessAlerts(int startIndex, int batchSize) {

		ResultBean rs = null;
		try {
			rs = Utils.validatePagination(startIndex, batchSize);

			if (rs != null) {
				return Utils.convertToJson(rs);
			}
			LOG.info("Getting alert");
			ProcessDao processDao = ProcessDao.getDao(peService);
			LOG.info("Process dao : " + processDao);
			ReportDao reportDao = ReportDao.getDao(paService, contentService);
			LOG.info("Report dao : " + processDao);

			Long[] processModelIds = reportDao.getAllProcessModelsId();
			rs = processDao.getTaskSummaryForProcessModels(processModelIds, startIndex, batchSize);
			return Utils.convertToJson(rs);

		} catch (Exception ex) {
			LOG.debug(ex);
			return Utils.convertToJson(new ResultBean(true, ex.getMessage()));
		}
	}

	/**
	 * Gets the process alerts by filters.
	 *
	 * @param searchFilters the search filters
	 * @param aggregate the aggregate
	 * @return the process alerts by filters
	 * @throws PrivilegeException the privilege exception
	 * @throws InvalidContentException the invalid content exception
	 * @throws UnsupportedReportSpecificationException the unsupported report specification exception
	 * @throws ReportComplexityException the report complexity exception
	 * @throws ReportSizeException the report size exception
	 * @throws ExpressionException the expression exception
	 */
	public String getProcessAlertsByFilters(String[] searchFilters, boolean aggregate)
			throws PrivilegeException, InvalidContentException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {

		ResultBean rs = null;

		if (!Utils.isValidSearchFilter(searchFilters)) {
			rs = new ResultBean(true, "Syntax error in search filter(s), please refer documentation");
			return Utils.convertToJson(rs);
		}
		LOG.info("Getting alert");
		ProcessDao processDao = ProcessDao.getDao(peService);
		LOG.info("Process dao : " + processDao);
		ReportDao reportDao = ReportDao.getDao(paService, contentService);
		LOG.info("Report dao : " + processDao);

		Long[] processModelIds = reportDao.getAllProcessModelsId();
		rs = processDao.getTaskSummaryForProcessModels(processModelIds, searchFilters, aggregate);

		return Utils.convertToJson(rs);
	}


}