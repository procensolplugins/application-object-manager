/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class AppianObjectsCollisionBean.
 */
public class AppianObjectsCollisionBean {

	/** The object uuid. */
	private String objectUuid;
	
	/** The type. */
	private String type;
	
	/** The application UUI ds. */
	private Set<String> applicationUUIDs;
	
	/** The application names. */
	private Set<String> applicationNames;
	
	/** The appian obj bean. */
	private AppianObjectsBean appianObjBean;
	
	/** The is colided. */
	private boolean isColided;
		
	/**
	 * Instantiates a new appian objects collision bean.
	 *
	 * @param type the type
	 * @param objectUuid the object uuid
	 */
	public AppianObjectsCollisionBean(String type, Object objectUuid) {
		super();
		this.objectUuid = String.valueOf(objectUuid);
		this.type = type;
	}
	
//	public AppianObjectsCollisionBean(String objectUuid, String type, Set<String> applicationUUIDs,
//			AppianObjectsBean appianObjBean, List<ApplicationBean> associatedApplications) {
//		super();
//		this.objectUuid = objectUuid;
//		this.type = type;
//		this.applicationUUIDs = applicationUUIDs;
//		this.appianObjBean = appianObjBean;
//		this.associatedApplications = associatedApplications;
//	}


	/**
 * Gets the object uuid.
 *
 * @return the object uuid
 */
public String getObjectUuid() {
		return objectUuid;
	}
	
	/**
	 * Sets the object uuid.
	 *
	 * @param objectUuid the new object uuid
	 */
	public void setObjectUuid(String objectUuid) {
		this.objectUuid = objectUuid;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Gets the application UUI ds.
	 *
	 * @return the application UUI ds
	 */
	public Set<String> getApplicationUUIDs() {
		return applicationUUIDs;
	}
	
	/**
	 * Sets the application UUI ds.
	 *
	 * @param applicationUUIDs the new application UUI ds
	 */
	public void setApplicationUUIDs(Set<String> applicationUUIDs) {
		this.applicationUUIDs = applicationUUIDs;
	}
	
	/**
	 * Gets the application names.
	 *
	 * @return the application names
	 */
	public Set<String> getApplicationNames() {
		return applicationNames;
	}
	
	/**
	 * Sets the application names.
	 *
	 * @param applicationNames the new application names
	 */
	public void setApplicationNames(Set<String> applicationNames) {
		this.applicationNames = applicationNames;
	}
	
	/**
	 * Gets the appian obj bean.
	 *
	 * @return the appian obj bean
	 */
	public AppianObjectsBean getAppianObjBean() {
		return appianObjBean;
	}
	
	/**
	 * Sets the appian obj bean.
	 *
	 * @param appianObjBean the new appian obj bean
	 */
	public void setAppianObjBean(AppianObjectsBean appianObjBean) {
		this.appianObjBean = appianObjBean;
	}

	/**
	 * Checks if is colided.
	 *
	 * @return true, if is colided
	 */
	public boolean isColided() {
		return isColided;
	}

	/**
	 * Sets the colided.
	 *
	 * @param isColided the new colided
	 */
	public void setColided(boolean isColided) {
		this.isColided = isColided;
	}

}
