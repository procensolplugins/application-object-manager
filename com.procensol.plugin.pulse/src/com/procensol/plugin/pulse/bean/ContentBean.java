/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

import java.util.Date;
import java.util.Map;

import com.appiancorp.suiteapi.common.RoleSet;

// TODO: Auto-generated Javadoc
/**
 * The Class ContentBean.
 */
public class ContentBean {
	
	/** The latest version id. */
	private Integer latestVersionId;
	
	/** The description. */
	private String description;
	
	/** The type. */
	private Integer type;
	
	/** The subtype. */
	private Integer subtype;
	
	/** The parent. */
	private Long parent;
	
	/** The parent uuid. */
	private String parentUuid;
	
	/** The parent name. */
	private String parentName;
	
	/** The parent type. */
	private Integer parentType;
	
	/** The log. */
	private Long log;
	
	/** The log name. */
	private String logName;
	
	/** The state. */
	private Integer state;
	
	/** The creator. */
	private String creator;
	
	/** The created timestamp. */
	private Date createdTimestamp;
	
	/** The updated timestamp. */
	private Date updatedTimestamp;
	
	/** The expiration timestamp. */
	private Date expirationTimestamp;
	
	/** The locked at. */
	private Date lockedAt;
	
	/** The size. */
	private Integer size;
	
	/** The number of versions. */
	private Long numberOfVersions;
	
	/** The attributes. */
	private Map<String, Object> attributes;
	
	/** The favorite. */
	private Boolean favorite;
	
	/** The uuid. */
	private String uuid;
	
	/** The action requested. */
	private Integer actionRequested;
	
	/** The action requester. */
	private String actionRequester;
	
	/** The original content id. */
	private Long originalContentId;
	
	/** The current content id. */
	private Long currentContentId;
	
	/** The display name. */
	private String displayName;
	
	/** The role set. */
	private RoleSet roleSet;
	
	/** The security. */
	private Integer security;
	
	/** The types of children. */
	private Integer typesOfChildren;
	
	/** The version id. */
	private Integer versionId;
	
	/** The visibility. */
	private Integer visibility;
	
	/** The last modified by. */
	private String lastModifiedBy;
	
	/** The locked by username. */
	private String lockedByUsername;
	
	/** The id. */
	private Long id;
	
	/** The name. */
	private String name;
	
	/** The depth. */
	private Integer depth;

	/**
	 * Gets the action requested.
	 *
	 * @return the action requested
	 */
	public Integer getActionRequested() {
		return actionRequested;
	}

	/**
	 * Gets the action requester.
	 *
	 * @return the action requester
	 */
	public String getActionRequester() {
		return actionRequester;
	}

	/**
	 * Gets the attributes.
	 *
	 * @return the attributes
	 */
	public Map<String, Object> getAttributes() {
		return attributes;
	}

	/**
	 * Gets the created timestamp.
	 *
	 * @return the created timestamp
	 */
	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	/**
	 * Gets the creator.
	 *
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * Gets the current content id.
	 *
	 * @return the current content id
	 */
	public Long getCurrentContentId() {
		return currentContentId;
	}

	/**
	 * Gets the depth.
	 *
	 * @return the depth
	 */
	public Integer getDepth() {
		return depth;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Gets the expiration timestamp.
	 *
	 * @return the expiration timestamp
	 */
	public Date getExpirationTimestamp() {
		return expirationTimestamp;
	}

	/**
	 * Gets the favorite.
	 *
	 * @return the favorite
	 */
	public Boolean getFavorite() {
		return favorite;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Gets the latest version id.
	 *
	 * @return the latest version id
	 */
	public Integer getLatestVersionId() {
		return latestVersionId;
	}

	/**
	 * Gets the locked at.
	 *
	 * @return the locked at
	 */
	public Date getLockedAt() {
		return lockedAt;
	}

	/**
	 * Gets the locked by username.
	 *
	 * @return the locked by username
	 */
	public String getLockedByUsername() {
		return lockedByUsername;
	}

	/**
	 * Gets the log.
	 *
	 * @return the log
	 */
	public Long getLog() {
		return log;
	}

	/**
	 * Gets the log name.
	 *
	 * @return the log name
	 */
	public String getLogName() {
		return logName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the number of versions.
	 *
	 * @return the number of versions
	 */
	public Long getNumberOfVersions() {
		return numberOfVersions;
	}

	/**
	 * Gets the original content id.
	 *
	 * @return the original content id
	 */
	public Long getOriginalContentId() {
		return originalContentId;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public Long getParent() {
		return parent;
	}

	/**
	 * Gets the parent name.
	 *
	 * @return the parent name
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * Gets the parent type.
	 *
	 * @return the parent type
	 */
	public Integer getParentType() {
		return parentType;
	}

	/**
	 * Gets the parent uuid.
	 *
	 * @return the parent uuid
	 */
	public String getParentUuid() {
		return parentUuid;
	}

	/**
	 * Gets the role set.
	 *
	 * @return the role set
	 */
	public RoleSet getRoleSet() {
		return roleSet;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Integer getSecurity() {
		return security;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Gets the subtype.
	 *
	 * @return the subtype
	 */
	public Integer getSubtype() {
		return subtype;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * Gets the types of children.
	 *
	 * @return the types of children
	 */
	public Integer getTypesOfChildren() {
		return typesOfChildren;
	}

	/**
	 * Gets the updated timestamp.
	 *
	 * @return the updated timestamp
	 */
	public Date getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * Gets the version id.
	 *
	 * @return the version id
	 */
	public Integer getVersionId() {
		return versionId;
	}

	/**
	 * Gets the visibility.
	 *
	 * @return the visibility
	 */
	public Integer getVisibility() {
		return visibility;
	}
	
	/**
	 * Sets the action requested.
	 *
	 * @param actionRequested the new action requested
	 */
	public void setActionRequested(Integer actionRequested) {
		this.actionRequested = actionRequested;
	}

	/**
	 * Sets the action requester.
	 *
	 * @param actionRequester the new action requester
	 */
	public void setActionRequester(String actionRequester) {
		this.actionRequester = actionRequester;
	}

	/**
	 * Sets the attributes.
	 *
	 * @param attributes the attributes
	 */
	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Sets the created timestamp.
	 *
	 * @param createdTimestamp the new created timestamp
	 */
	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	/**
	 * Sets the creator.
	 *
	 * @param creator the new creator
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * Sets the current content id.
	 *
	 * @param currentContentId the new current content id
	 */
	public void setCurrentContentId(Long currentContentId) {
		this.currentContentId = currentContentId;
	}

	/**
	 * Sets the depth.
	 *
	 * @param depth the new depth
	 */
	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the display name.
	 *
	 * @param displayName the new display name
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Sets the expiration timestamp.
	 *
	 * @param expirationTimestamp the new expiration timestamp
	 */
	public void setExpirationTimestamp(Date expirationTimestamp) {
		this.expirationTimestamp = expirationTimestamp;
	}

	/**
	 * Sets the favorite.
	 *
	 * @param favorite the new favorite
	 */
	public void setFavorite(Boolean favorite) {
		this.favorite = favorite;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Sets the latest version id.
	 *
	 * @param latestVersionId the new latest version id
	 */
	public void setLatestVersionId(Integer latestVersionId) {
		this.latestVersionId = latestVersionId;
	}

	/**
	 * Sets the locked at.
	 *
	 * @param lockedAt the new locked at
	 */
	public void setLockedAt(Date lockedAt) {
		this.lockedAt = lockedAt;
	}

	/**
	 * Sets the locked by username.
	 *
	 * @param lockedByUsername the new locked by username
	 */
	public void setLockedByUsername(String lockedByUsername) {
		this.lockedByUsername = lockedByUsername;
	}

	/**
	 * Sets the log.
	 *
	 * @param log the new log
	 */
	public void setLog(Long log) {
		this.log = log;
	}

	/**
	 * Sets the log name.
	 *
	 * @param logName the new log name
	 */
	public void setLogName(String logName) {
		this.logName = logName;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the number of versions.
	 *
	 * @param numberOfVersions the new number of versions
	 */
	public void setNumberOfVersions(Long numberOfVersions) {
		this.numberOfVersions = numberOfVersions;
	}

	/**
	 * Sets the original content id.
	 *
	 * @param originalContentId the new original content id
	 */
	public void setOriginalContentId(Long originalContentId) {
		this.originalContentId = originalContentId;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(Long parent) {
		this.parent = parent;
	}

	/**
	 * Sets the parent name.
	 *
	 * @param parentName the new parent name
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * Sets the parent type.
	 *
	 * @param parentType the new parent type
	 */
	public void setParentType(Integer parentType) {
		this.parentType = parentType;
	}

	/**
	 * Sets the parent uuid.
	 *
	 * @param parentUuid the new parent uuid
	 */
	public void setParentUuid(String parentUuid) {
		this.parentUuid = parentUuid;
	}

	/**
	 * Sets the role set.
	 *
	 * @param roleSet the new role set
	 */
	public void setRoleSet(RoleSet roleSet) {
		this.roleSet = roleSet;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Integer security) {
		this.security = security;
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(Integer size) {
		this.size = size;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Sets the subtype.
	 *
	 * @param subtype the new subtype
	 */
	public void setSubtype(Integer subtype) {
		this.subtype = subtype;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * Sets the types of children.
	 *
	 * @param typesOfChildren the new types of children
	 */
	public void setTypesOfChildren(Integer typesOfChildren) {
		this.typesOfChildren = typesOfChildren;
	}

	/**
	 * Sets the updated timestamp.
	 *
	 * @param updatedTimestamp the new updated timestamp
	 */
	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uuid the new uuid
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * Sets the version id.
	 *
	 * @param versionId the new version id
	 */
	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	/**
	 * Sets the visibility.
	 *
	 * @param visibility the new visibility
	 */
	public void setVisibility(Integer visibility) {
		this.visibility = visibility;
	}

}