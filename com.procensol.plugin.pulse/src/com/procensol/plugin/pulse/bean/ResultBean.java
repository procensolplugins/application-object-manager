/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class ResultBean.
 */
public class ResultBean {

	/** The error occurred. */
	private boolean errorOccurred;

	/** The error. */
	private String error;

	/** The total count. */
	private long totalCount;

	/** The data. */
	private Object data;

	/**
	 * Instantiates a new result bean.
	 *
	 * @param errorOccurred
	 *            the error occurred
	 * @param error
	 *            the error
	 */
	public ResultBean(boolean errorOccurred, String error) {
		this.errorOccurred = errorOccurred;
		this.error = error;
		this.totalCount = 0L;
		this.data = new Object();
	}

	/**
	 * Instantiates a new result bean.
	 *
	 * @param totalCount
	 *            the total count
	 * @param data
	 *            the data
	 */
	public ResultBean(long totalCount, Object data) {
		this.errorOccurred = false;
		this.error = null;
		this.totalCount = totalCount;
		this.data = data;
	}

	/**
	 * Instantiates a new result bean.
	 *
	 * @param errorOccurred
	 *            the error occurred
	 * @param error
	 *            the error
	 * @param totalCount
	 *            the total count
	 * @param data
	 *            the data
	 */
	public ResultBean(boolean errorOccurred, String error, int totalCount, Object data) {
		super();
		this.errorOccurred = errorOccurred;
		this.error = error;
		this.totalCount = totalCount;
		this.data = data;
	}

	/**
	 * Checks if is error occurred.
	 *
	 * @return true, if is error occurred
	 */
	public boolean isErrorOccurred() {
		return errorOccurred;
	}

	/**
	 * Sets the error occurred.
	 *
	 * @param errorOccurred
	 *            the new error occurred
	 */
	public void setErrorOccurred(boolean errorOccurred) {
		this.errorOccurred = errorOccurred;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * Sets the error.
	 *
	 * @param error
	 *            the new error
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Gets the total count.
	 *
	 * @return the total count
	 */
	public long getTotalCount() {
		return totalCount;
	}

	/**
	 * Sets the total count.
	 *
	 * @param totalCount
	 *            the new total count
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data
	 *            the new data
	 */
	public void setData(Object data) {
		this.data = data;
	}

}
