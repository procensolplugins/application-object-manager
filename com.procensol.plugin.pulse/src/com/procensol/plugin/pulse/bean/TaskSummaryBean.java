/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

import java.util.Date;

import com.appiancorp.suiteapi.process.ProcessException;
import com.appiancorp.suiteapi.process.ProcessExceptionType;
import com.appiancorp.suiteapi.process.TaskSummary;

// TODO: Auto-generated Javadoc
/**
 * The Class TaskSummaryBean.
 */
public class TaskSummaryBean {
	
	/** The task name. */
	public static String TASK_NAME = "taskName";
	
	/** The process name. */
	public static String PROCESS_NAME = "processName";
	
	/** The status time. */
	public static String STATUS_TIME = "statusTime";
	

	/** The process ID. */
	private Long processID;

	/** The process name. */
	private String processName;

	/** The task id. */
	private Long taskId;

	/** The task name. */
	private String taskName;

	/** The accepted time. */
	private Date acceptedTime;

	/** The task description. */
	private String taskDescription;

	/** The task display name. */
	private String taskDisplayName;

	/** The process initiator. */
	private String processInitiator;

	/** The process status. */
	private Integer processStatus;

	/** The status by. */
	private String statusBy;

	/** The accept mode. */
	private Integer acceptMode;
	
	/** The attached to type. */
	private Integer attachedToType;

	/** The status time. */
	private Date statusTime;

	/** The elapsed milliseconds. */
	private Long elapsedMilliseconds;

	/** The task exception resolver. */
	private String taskExceptionResolver;

	/** The task exception description. */
	private String taskExceptionDescription;

	/** The task exception note content. */
	private String taskExceptionNoteContent;

	/** The task exception recommended action. */
	private String taskExceptionRecommendedAction;

	/**
	 * Instantiates a new task summary bean.
	 */
	///////////
	public TaskSummaryBean() {
	}

	/**
	 * Instantiates a new task summary bean.
	 *
	 * @param taskSummary
	 *            the task summary
	 */
	public TaskSummaryBean(TaskSummary taskSummary) {

		ProcessException processException = taskSummary.getException();

		this.setStatusTime(taskSummary.getStatusTime());
		this.setTaskId(taskSummary.getId());
		this.setTaskName(taskSummary.getName());
		this.setAcceptedTime(taskSummary.getAcceptedTime());
		this.setAcceptMode(taskSummary.getAcceptMode());
		this.setElapsedMilliseconds(taskSummary.getElapsedMilliseconds());
		this.setProcessID(taskSummary.getProcessId());
		this.setProcessInitiator(taskSummary.getProcessInitiator());
		this.setProcessName(taskSummary.getProcessName());
		this.setProcessStatus(taskSummary.getProcessStatus());
		this.setStatusBy(taskSummary.getStatusBy());
		this.setTaskDescription(taskSummary.getDescription());
		this.setTaskDisplayName(taskSummary.getDisplayName());

		if (processException != null) {
			ProcessExceptionType peExpType = processException.getExceptionType();
			this.setAttachedToType(peExpType.getAttachedToType());
			this.setTaskExceptionResolver(processException.getResolver());
			this.setTaskExceptionDescription(peExpType.getDescription());
			this.setTaskExceptionNoteContent(peExpType.getNoteContent());
			this.setTaskExceptionRecommendedAction(peExpType.getRecommendedAction());
		}
	}

	///////////////

	/**
	 * Gets the attached to type.
	 *
	 * @return the attached to type
	 */
	public Integer getAttachedToType() {
		return attachedToType;
	}

	/**
	 * Sets the attached to type.
	 *
	 * @param attachedToType the new attached to type
	 */
	public void setAttachedToType(Integer attachedToType) {
		this.attachedToType = attachedToType;
	}

	/**
	 * Gets the process ID.
	 *
	 * @return the process ID
	 */
	public Long getProcessID() {
		return processID;
	}

	/**
	 * Sets the process ID.
	 *
	 * @param processID
	 *            the new process ID
	 */
	public void setProcessID(Long processID) {
		this.processID = processID;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the process name
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName
	 *            the new process name
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public Long getTaskId() {
		return taskId;
	}

	/**
	 * Sets the task id.
	 *
	 * @param taskId
	 *            the new task id
	 */
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	/**
	 * Gets the task name.
	 *
	 * @return the task name
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * Sets the task name.
	 *
	 * @param taskName
	 *            the new task name
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * Gets the accepted time.
	 *
	 * @return the accepted time
	 */
	public Date getAcceptedTime() {
		return acceptedTime;
	}

	/**
	 * Sets the accepted time.
	 *
	 * @param acceptedTime
	 *            the new accepted time
	 */
	public void setAcceptedTime(Date acceptedTime) {
		this.acceptedTime = acceptedTime;
	}

	/**
	 * Gets the task description.
	 *
	 * @return the task description
	 */
	public String getTaskDescription() {
		return taskDescription;
	}

	/**
	 * Sets the task description.
	 *
	 * @param taskDescription
	 *            the new task description
	 */
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	/**
	 * Gets the task display name.
	 *
	 * @return the task display name
	 */
	public String getTaskDisplayName() {
		return taskDisplayName;
	}

	/**
	 * Sets the task display name.
	 *
	 * @param taskDisplayName
	 *            the new task display name
	 */
	public void setTaskDisplayName(String taskDisplayName) {
		this.taskDisplayName = taskDisplayName;
	}

	/**
	 * Gets the process initiator.
	 *
	 * @return the process initiator
	 */
	public String getProcessInitiator() {
		return processInitiator;
	}

	/**
	 * Sets the process initiator.
	 *
	 * @param processInitiator
	 *            the new process initiator
	 */
	public void setProcessInitiator(String processInitiator) {
		this.processInitiator = processInitiator;
	}

	/**
	 * Gets the process status.
	 *
	 * @return the process status
	 */
	public Integer getProcessStatus() {
		return processStatus;
	}

	/**
	 * Sets the process status.
	 *
	 * @param processStatus
	 *            the new process status
	 */
	public void setProcessStatus(Integer processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * Gets the status by.
	 *
	 * @return the status by
	 */
	public String getStatusBy() {
		return statusBy;
	}

	/**
	 * Sets the status by.
	 *
	 * @param statusBy
	 *            the new status by
	 */
	public void setStatusBy(String statusBy) {
		this.statusBy = statusBy;
	}

	/**
	 * Gets the status time.
	 *
	 * @return the status time
	 */
	public Date getStatusTime() {
		return statusTime;
	}

	/**
	 * Sets the status time.
	 *
	 * @param statusTime the new status time
	 */
	public void setStatusTime(Date statusTime) {
		this.statusTime = statusTime;
	}

	/**
	 * Gets the accept mode.
	 *
	 * @return the accept mode
	 */
	public Integer getAcceptMode() {
		return acceptMode;
	}

	/**
	 * Sets the accept mode.
	 *
	 * @param acceptMode
	 *            the new accept mode
	 */
	public void setAcceptMode(int acceptMode) {
		this.acceptMode = acceptMode;
	}

	/**
	 * Gets the elapsed milliseconds.
	 *
	 * @return the elapsed milliseconds
	 */
	public Long getElapsedMilliseconds() {
		return elapsedMilliseconds;
	}

	/**
	 * Sets the elapsed milliseconds.
	 *
	 * @param elapsedMilliseconds
	 *            the new elapsed milliseconds
	 */
	public void setElapsedMilliseconds(Long elapsedMilliseconds) {
		this.elapsedMilliseconds = elapsedMilliseconds;
	}

	/**
	 * Gets the task exception resolver.
	 *
	 * @return the task exception resolver
	 */
	public String getTaskExceptionResolver() {
		return taskExceptionResolver;
	}

	/**
	 * Sets the task exception resolver.
	 *
	 * @param taskExceptionResolver
	 *            the new task exception resolver
	 */
	public void setTaskExceptionResolver(String taskExceptionResolver) {
		this.taskExceptionResolver = taskExceptionResolver;
	}

	/**
	 * Gets the task exception description.
	 *
	 * @return the task exception description
	 */
	public String getTaskExceptionDescription() {
		return taskExceptionDescription;
	}

	/**
	 * Sets the task exception description.
	 *
	 * @param taskExceptionDescription
	 *            the new task exception description
	 */
	public void setTaskExceptionDescription(String taskExceptionDescription) {
		this.taskExceptionDescription = taskExceptionDescription;
	}

	/**
	 * Gets the task exception note content.
	 *
	 * @return the task exception note content
	 */
	public String getTaskExceptionNoteContent() {
		return taskExceptionNoteContent;
	}

	/**
	 * Sets the task exception note content.
	 *
	 * @param taskExceptionNoteContent
	 *            the new task exception note content
	 */
	public void setTaskExceptionNoteContent(String taskExceptionNoteContent) {
		this.taskExceptionNoteContent = taskExceptionNoteContent;
	}

	/**
	 * Gets the task exception recommended action.
	 *
	 * @return the task exception recommended action
	 */
	public String getTaskExceptionRecommendedAction() {
		return taskExceptionRecommendedAction;
	}

	/**
	 * Sets the task exception recommended action.
	 *
	 * @param taskExceptionRecommendedAction
	 *            the new task exception recommended action
	 */
	public void setTaskExceptionRecommendedAction(String taskExceptionRecommendedAction) {
		this.taskExceptionRecommendedAction = taskExceptionRecommendedAction;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "TaskSummaryBean [processID=" + processID + ", processName=" + processName + ", taskId=" + taskId
				+ ", taskName=" + taskName + ", acceptedTime=" + acceptedTime + ", taskDescription=" + taskDescription
				+ ", taskDisplayName=" + taskDisplayName + ", processInitiator=" + processInitiator + ", processStatus="
				+ processStatus + ", statusBy=" + statusBy + ", acceptMode=" + acceptMode + ", taskExceptionResolver="
				+ taskExceptionResolver + ", taskExceptionDescription=" + taskExceptionDescription
				+ ", taskExceptionNoteContent=" + taskExceptionNoteContent + ", taskExceptionRecommendedAction="
				+ taskExceptionRecommendedAction + ", attachedToType=" + attachedToType + "]";
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + attachedToType;
		result = prime * result + ((processName == null) ? 0 : processName.hashCode());
		result = prime * result + processStatus;
		result = prime * result + ((taskExceptionDescription == null) ? 0 : taskExceptionDescription.hashCode());
		result = prime * result + ((taskExceptionNoteContent == null) ? 0 : taskExceptionNoteContent.hashCode());
		result = prime * result
				+ ((taskExceptionRecommendedAction == null) ? 0 : taskExceptionRecommendedAction.hashCode());
		result = prime * result + ((taskName == null) ? 0 : taskName.hashCode());
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TaskSummaryBean other = (TaskSummaryBean) obj;
		if (attachedToType != other.attachedToType) {
			return false;
		}
		if (processName == null) {
			if (other.processName != null) {
				return false;
			}
		} else if (!processName.equals(other.processName)) {
			return false;
		}
		if (processStatus != other.processStatus) {
			return false;
		}
		if (taskExceptionDescription == null) {
			if (other.taskExceptionDescription != null) {
				return false;
			}
		} else if (!taskExceptionDescription.equals(other.taskExceptionDescription)) {
			return false;
		}
		if (taskExceptionNoteContent == null) {
			if (other.taskExceptionNoteContent != null) {
				return false;
			}
		} else if (!taskExceptionNoteContent.equals(other.taskExceptionNoteContent)) {
			return false;
		}
		if (taskExceptionRecommendedAction == null) {
			if (other.taskExceptionRecommendedAction != null) {
				return false;
			}
		} else if (!taskExceptionRecommendedAction.equals(other.taskExceptionRecommendedAction)) {
			return false;
		}
		if (taskName == null) {
			if (other.taskName != null) {
				return false;
			}
		} else if (!taskName.equals(other.taskName)) {
			return false;
		}
		return true;
	}

}