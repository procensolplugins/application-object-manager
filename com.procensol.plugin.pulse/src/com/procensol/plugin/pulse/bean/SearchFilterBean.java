/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

import java.util.Arrays;

import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchFilterBean.
 */
public class SearchFilterBean {

	/** The Constant FIELD_NAME. */
	public static final String FIELD_NAME = "fieldName";
	
	/** The Constant OPERATION. */
	public static final String OPERATION = "operation";
	
	/** The Constant OPRANDS. */
	public static final String OPRANDS = "oprands";

	/** The field name. */
	private String fieldName;
	
	/** The operation. */
	private String operation;
	
	/** The oprands. */
	private String[] oprands;

	/**
	 * Instantiates a new search filter bean.
	 */
	public SearchFilterBean() {
	}

	/**
	 * Instantiates a new search filter bean.
	 *
	 * @param fieldName the field name
	 * @param operation the operation
	 * @param oprands the oprands
	 */
	public SearchFilterBean(String fieldName, String operation, String[] oprands) {
		this.fieldName = fieldName;
		this.operation = operation;
		this.oprands = oprands;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * Sets the operation.
	 *
	 * @param operation the new operation
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * Gets the oprands.
	 *
	 * @return the oprands
	 */
	public String[] getOprands() {
		return oprands;
	}

	/**
	 * Sets the oprands.
	 *
	 * @param oprands the new oprands
	 */
	public void setOprands(String[] oprands) {
		this.oprands = oprands;
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		result = prime * result + Arrays.hashCode(oprands);
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SearchFilterBean other = (SearchFilterBean) obj;
		if (fieldName == null) {
			if (other.fieldName != null) {
				return false;
			}
		} else if (!fieldName.equals(other.fieldName)) {
			return false;
		}
		if (operation == null) {
			if (other.operation != null) {
				return false;
			}
		} else if (!operation.equals(other.operation)) {
			return false;
		}
		if (!Arrays.equals(oprands, other.oprands)) {
			return false;
		}
		return true;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SearchFilterBean [fieldName=" + fieldName + ", operation=" + operation + ", oprands="
				+ Arrays.toString(oprands) + "]";
	}

	/**
	 * Builds the me.
	 *
	 * @param searchFilterTxt the search filter txt
	 */
	public void buildMe(String searchFilterTxt) {

		searchFilterTxt = Utils.sanitize(searchFilterTxt, "[^ a-zA-Z0-9;:,//]", "");

		String keyValuesArr[] = searchFilterTxt.split(";");
		for (String keyValue : keyValuesArr) {
			String[] keyValueArr = keyValue.split(":");
			if (keyValueArr[0].equalsIgnoreCase(SearchFilterBean.FIELD_NAME)) {
				this.setFieldName(keyValueArr[1]);
			} else if (keyValueArr[0].equalsIgnoreCase(SearchFilterBean.OPERATION)) {
				this.setOperation(keyValueArr[1]);
			} else if (keyValueArr[0].equalsIgnoreCase(SearchFilterBean.OPRANDS)) {
				String[] oprands = keyValueArr[1].split(",");
				this.setOprands(oprands);
			}
		}
	}
}
