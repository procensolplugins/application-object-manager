/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

import java.util.Arrays;

import com.appiancorp.suiteapi.applications.Application;
import com.appiancorp.suiteapi.applications.Application.ApplicationActions;
import com.appiancorp.suiteapi.applications.Application.ApplicationNavigation;
import com.appiancorp.suiteapi.applications.Application.AssociatedApplications;
import com.appiancorp.suiteapi.applications.Application.AssociatedObjects;
import com.appiancorp.suiteapi.applications.ApplicationAction;
import com.appiancorp.suiteapi.applications.ApplicationNavigationItem;

// TODO: Auto-generated Javadoc
/**
 * The Class ApplicationBean.
 */
public class ApplicationBean extends ContentBean {

	/** The url identifier. */
	private String urlIdentifier;
	
	/** The company name. */
	private String companyName;
	
	/** The company url. */
	private String companyUrl;
	
	/** The published. */
	private Boolean published;
	
	/** The icon relative path. */
	private String iconRelativePath;
	
	/** The associated objects. */
	private AssociatedObjects associatedObjects;
	
	/** The application navigation. */
	private ApplicationNavigation applicationNavigation;
	
	/** The application navigation items. */
	private ApplicationNavigationItem[] applicationNavigationItems;
	
	/** The associated applications. */
	private AssociatedApplications associatedApplications;
	
	/** The application actions. */
	private ApplicationActions applicationActions;
	
	/** The app actions. */
	private ApplicationAction[] appActions;

	/** The associated objects count. */
	private Integer associatedObjectsCount;
	
	/** The application navigation count. */
	private Integer applicationNavigationCount;
	
	/** The application navigation items count. */
	private Integer applicationNavigationItemsCount;
	
	/** The associated applications count. */
	private Integer associatedApplicationsCount;
	
	/** The application actions count. */
	private Integer applicationActionsCount;
	
	/** The app actions count. */
	private Integer appActionsCount;


	/**
	 * Instantiates a new application bean.
	 *
	 * @param appObj the app obj
	 * @param partial the partial
	 */
	public ApplicationBean(Application appObj, Boolean partial) {

		if (!partial) {
			this.setAppActions(appObj.getActions());
			this.setApplicationActions(appObj.getApplicationActions());
			this.setApplicationNavigation(appObj.getApplicationNavigation());
			this.setAssociatedApplications(appObj.getAssociatedApplications());
			this.setApplicationNavigationItems(appObj.getNavigationItems());
			this.setAssociatedObjects(appObj.getAssociatedObjects());
		}

		this.setCompanyName(appObj.getCompanyName());
		this.setCompanyUrl(appObj.getCompanyUrl());
		this.setIconRelativePath(appObj.getIconRelativePath());
		this.setUrlIdentifier(appObj.getUrlIdentifier());

		this.setDisplayName(appObj.getDisplayName());
		this.setRoleSet(appObj.getRoleSet());
		this.setSecurity(appObj.getSecurity());
		this.setTypesOfChildren(appObj.getTypesOfChildren());
		this.setVersionId(appObj.getVersionId());
		this.setVisibility(appObj.getVisibility());
		this.setLastModifiedBy(appObj.getLastModifiedBy());
		this.setLockedByUsername(appObj.getLockedByUsername());
		this.setId(appObj.getId());
		this.setName(appObj.getName());
		this.setDepth(appObj.getDepth());

		this.setActionRequested(appObj.getActionRequested());
		this.setActionRequester(appObj.getActionRequester());
		//this.setAttributes(appObj.getAttributes());
		this.setCreatedTimestamp(appObj.getCreatedTimestamp());
		this.setCreator(appObj.getCreator());
		this.setCurrentContentId(appObj.getCurrentContentId());
		this.setDescription(appObj.getDescription());
		this.setExpirationTimestamp(appObj.getExpirationTimestamp());
		this.setFavorite(appObj.getFavorite());
		this.setLatestVersionId(appObj.getLatestVersionId());
		this.setLockedAt(appObj.getLockedAt());
		this.setLog(appObj.getLog());
		this.setLogName(appObj.getLogName());
		this.setNumberOfVersions(appObj.getNumberOfVersions());
		this.setOriginalContentId(appObj.getOriginalContentId());
		this.setParent(appObj.getParent());
		this.setParentName(appObj.getParentName());
		this.setParentType(appObj.getParentType());
		this.setSize(appObj.getSize());
		this.setType(appObj.getType());
		this.setUpdatedTimestamp(appObj.getUpdatedTimestamp());
		this.setUuid(appObj.getUuid());

	}

	/**
	 * Gets the url identifier.
	 *
	 * @return the url identifier
	 */
	public String getUrlIdentifier() {
		return urlIdentifier;
	}

	/**
	 * Sets the url identifier.
	 *
	 * @param urlIdentifier the new url identifier
	 */
	public void setUrlIdentifier(String urlIdentifier) {
		this.urlIdentifier = urlIdentifier;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Gets the company url.
	 *
	 * @return the company url
	 */
	public String getCompanyUrl() {
		return companyUrl;
	}

	/**
	 * Sets the company url.
	 *
	 * @param companyUrl the new company url
	 */
	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	/**
	 * Checks if is published.
	 *
	 * @return the boolean
	 */
	public Boolean isPublished() {
		return published;
	}

	/**
	 * Sets the published.
	 *
	 * @param published the new published
	 */
	public void setPublished(Boolean published) {
		this.published = published;
	}

	/**
	 * Gets the icon relative path.
	 *
	 * @return the icon relative path
	 */
	public String getIconRelativePath() {
		return iconRelativePath;
	}

	/**
	 * Sets the icon relative path.
	 *
	 * @param iconRelativePath the new icon relative path
	 */
	public void setIconRelativePath(String iconRelativePath) {
		this.iconRelativePath = iconRelativePath;
	}

	/**
	 * Gets the associated objects.
	 *
	 * @return the associated objects
	 */
	public AssociatedObjects getAssociatedObjects() {
		return associatedObjects;
	}

	/**
	 * Sets the associated objects.
	 *
	 * @param associatedObjects the new associated objects
	 */
	public void setAssociatedObjects(AssociatedObjects associatedObjects) {
		this.associatedObjects = associatedObjects;
	}

	/**
	 * Gets the application navigation.
	 *
	 * @return the application navigation
	 */
	public ApplicationNavigation getApplicationNavigation() {
		return applicationNavigation;
	}

	/**
	 * Sets the application navigation.
	 *
	 * @param applicationNavigation the new application navigation
	 */
	public void setApplicationNavigation(ApplicationNavigation applicationNavigation) {
		this.applicationNavigation = applicationNavigation;
	}

	/**
	 * Gets the application navigation items.
	 *
	 * @return the application navigation items
	 */
	public ApplicationNavigationItem[] getApplicationNavigationItems() {
		return applicationNavigationItems;
	}

	/**
	 * Sets the application navigation items.
	 *
	 * @param applicationNavigationItems the new application navigation items
	 */
	public void setApplicationNavigationItems(ApplicationNavigationItem[] applicationNavigationItems) {
		this.applicationNavigationItems = applicationNavigationItems;
	}

	/**
	 * Gets the associated applications.
	 *
	 * @return the associated applications
	 */
	public AssociatedApplications getAssociatedApplications() {
		return associatedApplications;
	}

	/**
	 * Sets the associated applications.
	 *
	 * @param associatedApplications the new associated applications
	 */
	public void setAssociatedApplications(AssociatedApplications associatedApplications) {
		this.associatedApplications = associatedApplications;
	}

	/**
	 * Gets the application actions.
	 *
	 * @return the application actions
	 */
	public ApplicationActions getApplicationActions() {
		return applicationActions;
	}

	/**
	 * Sets the application actions.
	 *
	 * @param applicationActions the new application actions
	 */
	public void setApplicationActions(ApplicationActions applicationActions) {
		this.applicationActions = applicationActions;
	}

	/**
	 * Gets the app actions.
	 *
	 * @return the app actions
	 */
	public ApplicationAction[] getAppActions() {
		return appActions;
	}

	/**
	 * Sets the app actions.
	 *
	 * @param appActions the new app actions
	 */
	public void setAppActions(ApplicationAction[] appActions) {
		this.appActions = appActions;
	}


	/**
	 * Gets the associated objects count.
	 *
	 * @return the associated objects count
	 */
	public Integer getAssociatedObjectsCount() {
		return associatedObjectsCount;
	}

	/**
	 * Sets the associated objects count.
	 *
	 * @param associatedObjectsCount the new associated objects count
	 */
	public void setAssociatedObjectsCount(Integer associatedObjectsCount) {
		this.associatedObjectsCount = associatedObjectsCount;
	}

	/**
	 * Gets the application navigation count.
	 *
	 * @return the application navigation count
	 */
	public Integer getApplicationNavigationCount() {
		return applicationNavigationCount;
	}

	/**
	 * Sets the application navigation count.
	 *
	 * @param applicationNavigationCount the new application navigation count
	 */
	public void setApplicationNavigationCount(Integer applicationNavigationCount) {
		this.applicationNavigationCount = applicationNavigationCount;
	}

	/**
	 * Gets the application navigation items count.
	 *
	 * @return the application navigation items count
	 */
	public Integer getApplicationNavigationItemsCount() {
		return applicationNavigationItemsCount;
	}

	/**
	 * Sets the application navigation items count.
	 *
	 * @param applicationNavigationItemsCount the new application navigation items count
	 */
	public void setApplicationNavigationItemsCount(Integer applicationNavigationItemsCount) {
		this.applicationNavigationItemsCount = applicationNavigationItemsCount;
	}

	/**
	 * Gets the associated applications count.
	 *
	 * @return the associated applications count
	 */
	public Integer getAssociatedApplicationsCount() {
		return associatedApplicationsCount;
	}

	/**
	 * Sets the associated applications count.
	 *
	 * @param associatedApplicationsCount the new associated applications count
	 */
	public void setAssociatedApplicationsCount(Integer associatedApplicationsCount) {
		this.associatedApplicationsCount = associatedApplicationsCount;
	}

	/**
	 * Gets the application actions count.
	 *
	 * @return the application actions count
	 */
	public Integer getApplicationActionsCount() {
		return applicationActionsCount;
	}

	/**
	 * Sets the application actions count.
	 *
	 * @param applicationActionsCount the new application actions count
	 */
	public void setApplicationActionsCount(Integer applicationActionsCount) {
		this.applicationActionsCount = applicationActionsCount;
	}

	/**
	 * Gets the app actions count.
	 *
	 * @return the app actions count
	 */
	public Integer getAppActionsCount() {
		return appActionsCount;
	}

	/**
	 * Sets the app actions count.
	 *
	 * @param appActionsCount the new app actions count
	 */
	public void setAppActionsCount(Integer appActionsCount) {
		this.appActionsCount = appActionsCount;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ApplicationBean [urlIdentifier=" + urlIdentifier + ", companyName=" + companyName + ", companyUrl="
				+ companyUrl + ", published=" + published + ", iconRelativePath=" + iconRelativePath
				+ ", associatedObjects=" + associatedObjects + ", applicationNavigation=" + applicationNavigation
				+ ", applicationNavigationItems=" + Arrays.toString(applicationNavigationItems)
				+ ", associatedApplications=" + associatedApplications + ", applicationActions=" + applicationActions
				+ ", appActions=" + Arrays.toString(appActions) + "]";
	}

}