/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.bean;

import java.util.Date;

import com.appiancorp.suiteapi.process.ProcessModel;

// TODO: Auto-generated Javadoc
/**
 * The Class AppianObjectsBean.
 */
public class AppianObjectsBean {

	/** The type. */
	private String type;
	
	/** The id. */
	private Long id;
	
	/** The uuid. */
	private String uuid;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The total number of versions. */
	private Long totalNumberOfVersions;
	
	/** The folder path. */
	private String folderPath;
	
	/** The parent name. */
	private String parentName;
	
	/** The parent id. */
	private Long parentId;
	
	/** The is public. */
	private boolean isPublic;
	
	/** The last modified by. */
	private String lastModifiedBy;
	
	/** The last modified on. */
	private Date lastModifiedOn;
	
	/** The created by. */
	private String createdBy;
	
	/** The created on. */
	private Date createdOn;

	/**
	 * Instantiates a new appian objects bean.
	 *
	 * @param pm the pm
	 */
	public AppianObjectsBean(ProcessModel pm) {
		this.setId(pm.getId());
		this.setUuid(pm.getUuid());
		this.setName(pm.toString());
		this.setDescription(pm.toString());
		this.setLastModifiedBy(pm.getLastModifiedUsername());
		this.setLastModifiedOn(pm.getTimeStampUpdated());
		this.setCreatedBy(pm.getCreatorUsername());
		this.setCreatedOn(pm.getTimeStampCreated());
	}

	/**
	 * Instantiates a new appian objects bean.
	 *
	 * @param type the type
	 * @param id the id
	 * @param uuid the uuid
	 * @param name the name
	 * @param description the description
	 * @param totalNumberOfVersions the total number of versions
	 * @param parentName the parent name
	 * @param parentId the parent id
	 * @param isPublic the is public
	 * @param lastModifiedBy the last modified by
	 * @param lastModifiedOn the last modified on
	 * @param createdBy the created by
	 * @param createdOn the created on
	 */
	public AppianObjectsBean(String type, Long id, String uuid, String name, String description,
			Long totalNumberOfVersions, String parentName, Long parentId, boolean isPublic, String lastModifiedBy,
			Date lastModifiedOn, String createdBy, Date createdOn) {
		super();
		this.type = type;
		this.id = id;
		this.uuid = uuid;
		this.name = name;
		this.description = description;
		this.totalNumberOfVersions = totalNumberOfVersions;
		this.parentName = parentName;
		this.parentId = parentId;
		this.isPublic = isPublic;
		this.lastModifiedBy = lastModifiedBy;
		this.lastModifiedOn = lastModifiedOn;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
	}
	


	/**
	 * Instantiates a new appian objects bean.
	 *
	 * @param type the type
	 * @param uuid the uuid
	 */
	public AppianObjectsBean(String type, String uuid) {
		super();
		this.type = type;
		this.uuid = uuid;
	}
	

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uuid the new uuid
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the total number of versions.
	 *
	 * @return the total number of versions
	 */
	public Long getTotalNumberOfVersions() {
		return totalNumberOfVersions;
	}

	/**
	 * Sets the total number of versions.
	 *
	 * @param totalNumberOfVersions the new total number of versions
	 */
	public void setTotalNumberOfVersions(Long totalNumberOfVersions) {
		this.totalNumberOfVersions = totalNumberOfVersions;
	}

	/**
	 * Gets the folder path.
	 *
	 * @return the folder path
	 */
	public String getFolderPath() {
		return folderPath;
	}

	/**
	 * Sets the folder path.
	 *
	 * @param folderPath the new folder path
	 */
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	/**
	 * Gets the parent id.
	 *
	 * @return the parent id
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * Sets the parent id.
	 *
	 * @param parentId the new parent id
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * Gets the parent name.
	 *
	 * @return the parent name
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * Sets the parent name.
	 *
	 * @param parentName the new parent name
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * Checks if is public.
	 *
	 * @return true, if is public
	 */
	public boolean isPublic() {
		return isPublic;
	}

	/**
	 * Sets the public.
	 *
	 * @param isPublic the new public
	 */
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
