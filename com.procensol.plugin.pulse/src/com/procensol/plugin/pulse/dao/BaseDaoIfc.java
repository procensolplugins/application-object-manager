/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao;

import com.procensol.plugin.pulse.bean.AppianObjectsBean;

// TODO: Auto-generated Javadoc
/**
 * The Interface BaseDaoIfc.
 */
public interface BaseDaoIfc {

	/**
	 * Gets the object.
	 *
	 * @param uuid the uuid
	 * @return the object
	 */
	public AppianObjectsBean getObject(Object uuid);
	
	/**
	 * Gets the object.
	 *
	 * @param key the key
	 * @param uuid the uuid
	 * @return the object
	 */
	public AppianObjectsBean getObject(String key, Object uuid);
}
