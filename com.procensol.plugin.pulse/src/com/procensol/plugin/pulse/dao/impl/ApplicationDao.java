/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import static com.procensol.plugin.pulse.utils.ConstantIfc.ADMINISTERED_PROPERTY_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.APPLICATION_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.CONNECTED_SYSTEM_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.CONTENT_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.DATATYPE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.DATA_SOURCE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.DATA_STORE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.EMBEDDED_SAIL_THEME_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.FORUM_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.GROUP_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.GROUP_TYPE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.PAGE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.PORTLET_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.PROCESS_MODEL_FOLDER_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.PROCESS_MODEL_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.RECORD_TYPE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.SITE_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.TASK_REPORT_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.TEMPO_FEED_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.TEMPO_REPORT_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.THIRD_PARTY_CREDENTIALS_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.USER_KEY;
import static com.procensol.plugin.pulse.utils.ConstantIfc.WEB_API_KEY;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.appiancorp.ix.GlobalIdMap;
import com.appiancorp.ix.Type;
import com.appiancorp.suiteapi.applications.Application;
import com.appiancorp.suiteapi.applications.Application.AssociatedObjects;
import com.appiancorp.suiteapi.applications.ApplicationNotFoundException;
import com.appiancorp.suiteapi.applications.ApplicationService;
import com.appiancorp.suiteapi.common.Constants;
import com.appiancorp.suiteapi.common.ResultPage;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.content.ContentConstants;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.process.ProcessDesignService;
import com.appiancorp.suiteapi.type.TypeService;
import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.bean.AppianObjectsCollisionBean;
import com.procensol.plugin.pulse.bean.ApplicationBean;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.utils.ConstantIfc;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantDao.
 */
public class ApplicationDao extends ContentDao {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ApplicationDao.class);

	/** The dao. */
	private static ApplicationDao dao;

	/** The pa service. */
	private ApplicationService appService;

	/** The pd service. */
	private ProcessDesignService pdService;

	/** The type service. */
	private TypeService typeService;

	/**
	 * Instantiates a new application dao.
	 *
	 * @param appService the app service
	 * @param contentService the content service
	 * @param pdService the pd service
	 * @param typeService the type service
	 */
	ApplicationDao(ApplicationService appService, ContentService contentService, ProcessDesignService pdService,
			TypeService typeService) {
		super(contentService);
		this.appService = appService;
		this.pdService = pdService;
		this.typeService = typeService;
	}

	/**
	 * Gets the dao.
	 *
	 * @param appService the app service
	 * @param contentService the content service
	 * @param pdService the pd service
	 * @param typeService the type service
	 * @return the dao
	 */
	public static ApplicationDao getDao(ApplicationService appService, ContentService contentService,
			ProcessDesignService pdService, TypeService typeService) {
		if (dao == null) {
			dao = new ApplicationDao(appService, contentService, pdService, typeService);
		}
		return dao;
	}

	/**
	 * Gets the all appliacations.
	 *
	 * @param startIndex the start index
	 * @param batchSize the batch size
	 * @param includeDraft the include draft
	 * @return the all appliacations
	 */
	public ResultBean getAllAppliacations(int startIndex, int batchSize, boolean includeDraft) {

		List<ApplicationBean> applicationObjectsList = new ArrayList<>();

		ResultPage resultPage = appService.getApplicationsPaging(startIndex, batchSize, ContentConstants.COLUMN_NAME,
				Constants.SORT_ORDER_DESCENDING, includeDraft);
		
		long totalCount = resultPage.getAvailableItems();
		
		Object[] applicationObjArr = resultPage.getResults();
		
		for (Object appObj : applicationObjArr) {
			try {
				ApplicationBean appBean = new ApplicationBean((Application) appObj, true);
				applicationObjectsList.add(appBean);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return new ResultBean(totalCount, applicationObjectsList);
	}

	/**
	 * Gets the application objects.
	 *
	 * @param applicationUuidOrId the application uuid or id
	 * @return the application objects
	 * @throws PrivilegeException the privilege exception
	 * @throws ApplicationNotFoundException the application not found exception
	 */
	public ResultBean getApplicationObjects(String applicationUuidOrId)
			throws PrivilegeException, ApplicationNotFoundException {
		

		Long id = null;
		try {
			id = Long.parseLong(applicationUuidOrId);
		} catch (NumberFormatException nfe) {

		}
		
		Application appObj = id==null ? appService.getApplication(applicationUuidOrId) : appService.getApplication(id);
		
		List<AppianObjectsBean> applicationObjectsList = new ArrayList<>();
		
		applicationObjectsList = getApplicationObjects(appObj.getAssociatedObjects());
		
		int totalCount = applicationObjectsList.size();
		return new ResultBean(totalCount, applicationObjectsList);
	}
//	
//	public ResultBean getApplicationObjects(Long applicationId)
//			throws PrivilegeException, ApplicationNotFoundException {
//		
//		Application appObj = appService.getApplication(applicationId);
//		
//		List<AppianObjectsBean> applicationObjectsList = new ArrayList<>();
//		
//		applicationObjectsList = getApplicationObjects(appObj.getAssociatedObjects());
//		
//		int totalCount = applicationObjectsList.size();
//		return new ResultBean(totalCount, applicationObjectsList);
//	}

	/**
 * Gets the application objects.
 *
 * @param associatedObjects the associated objects
 * @return the application objects
 */
public List<AppianObjectsBean> getApplicationObjects(AssociatedObjects associatedObjects) {
		
		List<AppianObjectsBean> list = new ArrayList<>();
		GlobalIdMap globalIdMap = associatedObjects.getGlobalIdMap();
		
		for (String key : ConstantIfc.APPIAN_OBJECT_KEYS) {
			Set<?> set = globalIdMap.get(Type.get(key));
			list.addAll(getApplicationObjects(set, key));
		}
		return list;
	}

	/**
	 * Gets the collided objects list.
	 *
	 * @param applicationUUIDsOrId the application UUI ds or id
	 * @return the collided objects list
	 * @throws PrivilegeException the privilege exception
	 * @throws ApplicationNotFoundException the application not found exception
	 */
	public List<AppianObjectsCollisionBean> getCollidedObjectsList(String[] applicationUUIDsOrId)
			throws PrivilegeException, ApplicationNotFoundException {

		List<AppianObjectsCollisionBean> appObjCollisionList = getAllCollidedObjectsList(applicationUUIDsOrId);
		
		return cleanAndRefillCollidedObjectsList(appObjCollisionList);
	}

	/**
	 * Clean and refill collided objects list.
	 *
	 * @param appObjCollisionList the app obj collision list
	 * @return the list
	 * @throws PrivilegeException the privilege exception
	 * @throws ApplicationNotFoundException the application not found exception
	 */
	private List<AppianObjectsCollisionBean> cleanAndRefillCollidedObjectsList(
			List<AppianObjectsCollisionBean> appObjCollisionList)
			throws PrivilegeException, ApplicationNotFoundException {

		List<AppianObjectsCollisionBean> listToReturn = new ArrayList<>();

		for (AppianObjectsCollisionBean appianObjectsCollisionBean : appObjCollisionList) {
			if (appianObjectsCollisionBean.isColided()) {
				appianObjectsCollisionBean.setAppianObjBean(
						getObject(appianObjectsCollisionBean.getType(), appianObjectsCollisionBean.getObjectUuid()));

				appianObjectsCollisionBean.setType(appianObjectsCollisionBean.getAppianObjBean().getType());
				listToReturn.add(appianObjectsCollisionBean);
			}
		}

		return listToReturn;
	}

	/**
	 * Gets the all collided objects list.
	 *
	 * @param applicationUUIDsOrId the application UUI ds or id
	 * @return the all collided objects list
	 * @throws PrivilegeException the privilege exception
	 * @throws ApplicationNotFoundException the application not found exception
	 */
	public List<AppianObjectsCollisionBean> getAllCollidedObjectsList(String[] applicationUUIDsOrId)
			throws PrivilegeException, ApplicationNotFoundException {

		HashMap<String, AppianObjectsCollisionBean> appObjCollisionMap = new HashMap<>();

		for (String applicationUuidOrId : applicationUUIDsOrId) {

			Long id = null;
			try {
				id = Long.parseLong(applicationUuidOrId);
			} catch (NumberFormatException nfe) {

			}
			
			Application appObj = (id == null) ? appService.getApplication(applicationUuidOrId)
					: appService.getApplication(id);

			GlobalIdMap globalIdMap = appObj.getAssociatedObjects().getGlobalIdMap();

			for (String objectTypeKey : ConstantIfc.APPIAN_OBJECT_KEYS) {

				Set<?> set = globalIdMap.get(Type.get(objectTypeKey));
				Iterator<?> objectsItr = set.iterator();

				while (objectsItr.hasNext()) {

					Object objectUUID = objectsItr.next();
					AppianObjectsCollisionBean appCollisionBeanObj = null;
					Set<String> applicationUUIDsHash = null;
					Set<String> applicationNamesHash = null;

					if (appObjCollisionMap.get(objectUUID) == null) {

						appCollisionBeanObj = new AppianObjectsCollisionBean(objectTypeKey, objectUUID);

						appCollisionBeanObj.setColided(false);
						applicationUUIDsHash = new HashSet<>();
						applicationUUIDsHash.add(appObj.getUuid());
						appCollisionBeanObj.setApplicationUUIDs(applicationUUIDsHash);

						applicationNamesHash = new HashSet<>();
						applicationNamesHash.add(appObj.getName());
						appCollisionBeanObj.setApplicationNames(applicationNamesHash);

						appObjCollisionMap.put(String.valueOf(objectUUID), appCollisionBeanObj);
					} else {
						appCollisionBeanObj = appObjCollisionMap.get(objectUUID);

						appCollisionBeanObj.setColided(true);
						applicationUUIDsHash = appCollisionBeanObj.getApplicationUUIDs();
						applicationUUIDsHash.add(appObj.getUuid());
						appCollisionBeanObj.setApplicationUUIDs(applicationUUIDsHash);

						applicationNamesHash = appCollisionBeanObj.getApplicationNames();
						applicationNamesHash.add(appObj.getName());
						appCollisionBeanObj.setApplicationNames(applicationNamesHash);

					}
				}
			}

		}

		return new ArrayList<AppianObjectsCollisionBean>(appObjCollisionMap.values());
	}
//
//	public List<String> getApplicationObjectsUUIDs(String applicationUUID, AssociatedObjects associatedObjects) {
//
//		HashMap<String, AppianObjectsCollisionBean> appObjCollisionMap = new HashMap<>();
//
//		List<String> list = new ArrayList<>();
//		GlobalIdMap globalIdMap = associatedObjects.getGlobalIdMap();
//
//		for (String key : ConstantIfc.APPIAN_OBJECT_KEYS) {
//			Set<?> set = globalIdMap.get(Type.get(key));
//			Iterator<?> itr = set.iterator();
//			while (itr.hasNext()) {
//				String uuid = (String) itr.next();
//				AppianObjectsCollisionBean appCollisionBeanObj = new AppianObjectsCollisionBean(uuid, key);
//			}
//		}
//
//		return list;
//	}

	/**
 * Gets the application objects.
 *
 * @param set the set
 * @param key the key
 * @return the application objects
 */
public List<AppianObjectsBean> getApplicationObjects(Set<?> set, String key) {

		List<AppianObjectsBean> list = new ArrayList<>();
		Iterator<?> itr = set.iterator();

		while (itr.hasNext()) {
			Object uuid = itr.next();
			AppianObjectsBean appObjBean = getObject(key, uuid);
			if (appObjBean != null) {
				list.add(appObjBean);
			}
		}
		return list;
	}

	/**
	 * Gets the object.
	 *
	 * @param key the key
	 * @param uuid the uuid
	 * @return the object
	 */
	public AppianObjectsBean getObject(String key, Object uuid) {

		switch (key) {
		case ADMINISTERED_PROPERTY_KEY:
		case GROUP_KEY:
		case GROUP_TYPE_KEY:
		case FORUM_KEY:
		case USER_KEY:
		case PORTLET_KEY:
		case PAGE_KEY:
		case APPLICATION_KEY:
		case TEMPO_FEED_KEY:
		case RECORD_TYPE_KEY:
		case TEMPO_REPORT_KEY:
		case TASK_REPORT_KEY:
		case WEB_API_KEY:
		case SITE_KEY:
		case THIRD_PARTY_CREDENTIALS_KEY:
		case DATA_SOURCE_KEY:
		case EMBEDDED_SAIL_THEME_KEY:
		case PROCESS_MODEL_FOLDER_KEY:
		case DATA_STORE_KEY:
		case CONNECTED_SYSTEM_KEY:
			return BaseDao.getDao().getObject(key, uuid);
		case CONTENT_KEY:
			return getDao(appService, contentService, pdService, typeService).getObject(uuid);
		case PROCESS_MODEL_KEY:
			return ProcessModelDao.getDao(pdService).getObject(uuid);
//		case DATATYPE_KEY:
//			return TypeDao.getDao(typeService).getObject(uuid);
		default:
			return null;
		}
	}

	/**
	 * Gets the application.
	 *
	 * @param uuidOrId the uuid or id
	 * @return the application
	 * @throws PrivilegeException the privilege exception
	 * @throws ApplicationNotFoundException the application not found exception
	 */
	public ResultBean getApplication(String uuidOrId) throws PrivilegeException, ApplicationNotFoundException {
		ResultBean rsb = new ResultBean(true, "Unknown Error");
		Long id = null;
		try {
			id = Long.parseLong(uuidOrId);
		} catch (NumberFormatException nfe) {

		}
		Application appObj = (id == null) ? appService.getApplication(uuidOrId) : appService.getApplication(id);
		ApplicationBean appObjBean = new ApplicationBean(appObj, false);
		rsb = new ResultBean(1, appObjBean);
		return rsb;
	}

}
