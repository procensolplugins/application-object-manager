/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.services.ServiceContextFactory;
import com.appiancorp.suiteapi.common.Constants;
import com.appiancorp.suiteapi.common.ServiceLocator;
import com.appiancorp.suiteapi.common.exceptions.ExpressionException;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.content.ContentConstants;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.content.exceptions.InvalidContentException;
import com.appiancorp.suiteapi.process.analytics2.ProcessAnalyticsService;
import com.appiancorp.suiteapi.process.analytics2.ProcessReport;
import com.appiancorp.suiteapi.process.analytics2.ReportData;
import com.appiancorp.suiteapi.process.exceptions.ReportComplexityException;
import com.appiancorp.suiteapi.process.exceptions.ReportSizeException;
import com.appiancorp.suiteapi.process.exceptions.UnsupportedReportSpecificationException;
import com.appiancorp.suiteapi.process.framework.SmartServiceContext;
import com.appiancorp.suiteapi.type.TypedValue;
import com.procensol.plugin.pulse.dao.BaseDaoIfc;
import com.procensol.plugin.pulse.manager.ProcessManager;
import com.procensol.plugin.pulse.utils.ConstantIfc;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantDao.
 */
@SuppressWarnings("deprecation")
public class ReportDao extends ContentDao implements BaseDaoIfc {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ProcessManager.class);

	/** The dao. */
	private static ReportDao dao;

	/** The pa service. */
	private ProcessAnalyticsService paService;

	/**
	 * Instantiates a new report dao.
	 *
	 * @param paService
	 *            the pa service
	 * @param contentService
	 *            the content service
	 */
	ReportDao(ProcessAnalyticsService paService, ContentService contentService) {
		super(contentService);
		this.paService = paService;
	}

	/**
	 * Gets the dao.
	 *
	 * @param paService
	 *            the pa service
	 * @param contentService
	 *            the content service
	 * @return the dao
	 */
	public static ReportDao getDao(ProcessAnalyticsService paService, ContentService contentService) {
		if (dao == null) {
			dao = new ReportDao(paService, contentService);
		}
		return dao;
	}

	/**
	 * Gets the dao.
	 *
	 * @param smartServiceCtx
	 *            the smart service ctx
	 * @return the dao
	 */
	@Deprecated
	public static ReportDao getDao(SmartServiceContext smartServiceCtx) {
		if (dao == null) {
			ServiceContext serviceContext = ServiceContextFactory.getServiceContext(smartServiceCtx.getUsername());
			ContentService cs = ServiceLocator.getContentService(serviceContext);
			ProcessAnalyticsService pas = ServiceLocator.getProcessAnalyticsService2(serviceContext);
			dao = new ReportDao(pas, cs);
		}
		return dao;
	}

	/**
	 * Gets the all process models id.
	 *
	 * @return the all process models id
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 * @throws UnsupportedReportSpecificationException
	 *             the unsupported report specification exception
	 * @throws ReportComplexityException
	 *             the report complexity exception
	 * @throws ReportSizeException
	 *             the report size exception
	 * @throws ExpressionException
	 *             the expression exception
	 */
	public Long[] getAllProcessModelsId()
			throws InvalidContentException, PrivilegeException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {

		ReportData allProcessInstanceReportData = getAllProcessModelsReportData();

		ArrayList<Map<String, Object>> reportColumnsMapList = getReportColumnsMapList(allProcessInstanceReportData);
		Long[] processModelIds = new Long[reportColumnsMapList.size()];

		LOG.info("Process model Ids are : ");
		for (int index = 0; index < reportColumnsMapList.size(); index++) {
			Map<String, Object> reportColumnsMap = reportColumnsMapList.get(index);
			processModelIds[index] = Long
					.valueOf(String.valueOf(reportColumnsMap.get(ConstantIfc.REPORT_COLUMN_PROCESSMODELID)));
			LOG.info(Long.valueOf(String.valueOf(reportColumnsMap.get(ConstantIfc.REPORT_COLUMN_PROCESSMODELID))));
		}

		return processModelIds;
	}

	/**
	 * Gets the all process models report data.
	 *
	 * @return the all process models report data
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 */
	public ReportData getAllProcessModelsReportData() throws InvalidContentException, PrivilegeException {
		return getReportData(ContentConstants.UUID_ALL_PROCESS_MODELS_REPORT);
	}

	/**
	 * Gets the report data.
	 *
	 * @param reportUUID
	 *            the report UUID
	 * @return the report data
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 */
	public ReportData getReportData(String reportUUID) throws InvalidContentException, PrivilegeException {
		ReportData reportData = null;
		ProcessReport processReport = paService.getProcessReport(contentService.getIdByUuid(reportUUID));
		reportData = processReport.getData();
		reportData.setBatchSize(Constants.COUNT_ALL);
		return reportData;
	}

	/**
	 * Gets the report data type value maps.
	 *
	 * @param reportData
	 *            the report data
	 * @return the report data type value maps
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 * @throws UnsupportedReportSpecificationException
	 *             the unsupported report specification exception
	 * @throws ReportComplexityException
	 *             the report complexity exception
	 * @throws ReportSizeException
	 *             the report size exception
	 * @throws ExpressionException
	 *             the expression exception
	 */
	public Object[] getReportDataTypeValueMaps(ReportData reportData)
			throws InvalidContentException, PrivilegeException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {
		return (Object[]) paService.getReportPageWithTypedValues(reportData).getResults();
	}

	/**
	 * Gets the report columns map list.
	 *
	 * @param reportUUID
	 *            the report UUID
	 * @return the report columns map list
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 * @throws UnsupportedReportSpecificationException
	 *             the unsupported report specification exception
	 * @throws ReportComplexityException
	 *             the report complexity exception
	 * @throws ReportSizeException
	 *             the report size exception
	 * @throws ExpressionException
	 *             the expression exception
	 */
	public ArrayList<Map<String, Object>> getReportColumnsMapList(String reportUUID)
			throws InvalidContentException, PrivilegeException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {

		ReportData reportData = getReportData(reportUUID);

		return getReportColumnsMapList(reportData);
	}

	/**
	 * Gets the report columns map list.
	 *
	 * @param reportUUID
	 *            the report UUID
	 * @param columnNamesList
	 *            the column names list
	 * @return the report columns map list
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 * @throws UnsupportedReportSpecificationException
	 *             the unsupported report specification exception
	 * @throws ReportComplexityException
	 *             the report complexity exception
	 * @throws ReportSizeException
	 *             the report size exception
	 * @throws ExpressionException
	 *             the expression exception
	 */
	public ArrayList<Map<String, Object>> getReportColumnsMapList(String reportUUID, List<String> columnNamesList)
			throws InvalidContentException, PrivilegeException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {

		ReportData reportData = getReportData(reportUUID);
		return getReportColumnsMapList(reportData, columnNamesList);
	}

	/**
	 * Gets the report columns map list.
	 *
	 * @param reportData
	 *            the report data
	 * @return the report columns map list
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 * @throws UnsupportedReportSpecificationException
	 *             the unsupported report specification exception
	 * @throws ReportComplexityException
	 *             the report complexity exception
	 * @throws ReportSizeException
	 *             the report size exception
	 * @throws ExpressionException
	 *             the expression exception
	 */
	public ArrayList<Map<String, Object>> getReportColumnsMapList(ReportData reportData)
			throws InvalidContentException, PrivilegeException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {
		return getReportColumnsMapList(reportData, null);
	}

	/**
	 * Gets the report columns map list.
	 *
	 * @param reportData
	 *            the report data
	 * @param columnNamesList
	 *            the column names list
	 * @return the report columns map list
	 * @throws InvalidContentException
	 *             the invalid content exception
	 * @throws PrivilegeException
	 *             the privilege exception
	 * @throws UnsupportedReportSpecificationException
	 *             the unsupported report specification exception
	 * @throws ReportComplexityException
	 *             the report complexity exception
	 * @throws ReportSizeException
	 *             the report size exception
	 * @throws ExpressionException
	 *             the expression exception
	 */
	public ArrayList<Map<String, Object>> getReportColumnsMapList(ReportData reportData, List<String> columnNamesList)
			throws InvalidContentException, PrivilegeException, UnsupportedReportSpecificationException,
			ReportComplexityException, ReportSizeException, ExpressionException {

		ArrayList<Map<String, Object>> reportColumnsMapList = new ArrayList<>();
		Map<String, Object> pvMap;

		Object[] tvArr = getReportDataTypeValueMaps(reportData);

		for (Object tv : tvArr) {
			HashMap<String, TypedValue> varMap = (HashMap<String, TypedValue>) tv;
			Set<String> reportColumnsKeySet = varMap.keySet();

			pvMap = new HashMap<String, Object>();
			TypedValue vaVal = null;
			String colValue = null;

			for (String colKey : reportColumnsKeySet) {
				if (columnNamesList == null || columnNamesList.contains(colKey)) {
					if (varMap.get(colKey) instanceof TypedValue) {
						vaVal = (TypedValue) varMap.get(colKey);
						if (vaVal != null) {
							Object value = vaVal.getValue();
							colValue = String.valueOf(value);
						}
					} else {
						colValue = String.valueOf(varMap.get(colKey));
					}
				} else {
					continue;
				}
				pvMap.put(colKey, colValue);
			}
			reportColumnsMapList.add(pvMap);
		}
		return reportColumnsMapList;
	}

}
