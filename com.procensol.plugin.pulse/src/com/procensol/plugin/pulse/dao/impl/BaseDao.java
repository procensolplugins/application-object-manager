/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;

import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.dao.BaseDaoIfc;


// TODO: Auto-generated Javadoc
/**
 * The Class ContentDao.
 */
public class BaseDao implements BaseDaoIfc {

	/**
	 * Instantiates a new base dao.
	 */
	BaseDao() {
		super();
	}

	/** The dao. */
	private static BaseDao dao = null;
	
	/**
	 * Gets the dao.
	 *
	 * @return the dao
	 */
	public static BaseDao getDao() {
		if (dao == null) {
			dao = new BaseDao();
		}
		return dao;
	}
	
	/**
	 * Gets the object.
	 *
	 * @param key the key
	 * @param uuid the uuid
	 * @return the object
	 */
	@Override
	
	public AppianObjectsBean getObject(String key, Object uuid) {

		try {
			String objectUUID = StringUtils.trimToEmpty(String.valueOf(uuid));
			AppianObjectsBean appObjBean = new AppianObjectsBean(key, objectUUID);

			return appObjBean;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Gets the object.
	 *
	 * @param uuid the uuid
	 * @return the object
	 */
	@Override
	public AppianObjectsBean getObject(Object uuid) {
		throw new  NotImplementedException(String.valueOf(uuid));
	}
}
