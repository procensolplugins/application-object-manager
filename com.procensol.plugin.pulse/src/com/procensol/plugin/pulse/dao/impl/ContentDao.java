/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import org.apache.commons.lang.StringUtils;

import com.appiancorp.suiteapi.common.exceptions.InvalidVersionException;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.content.Content;
import com.appiancorp.suiteapi.content.ContentConstants;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.content.exceptions.InvalidContentException;
import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.dao.BaseDaoIfc;
import com.procensol.plugin.pulse.utils.ConstantIfc;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ContentDao.
 */
public class ContentDao extends BaseDao implements BaseDaoIfc {

	/** The content service. */
	ContentService contentService;

	/**
	 * Instantiates a new content dao.
	 *
	 * @param contentService the content service
	 */
	ContentDao(ContentService contentService) {
		super();
		this.contentService = contentService;

	}

	/**
	 * Gets the object.
	 *
	 * @param uuid the uuid
	 * @return the object
	 */
	@Override
	public AppianObjectsBean getObject(Object uuid) {

		try {
			String objectUUID = StringUtils.trimToEmpty(String.valueOf(uuid));

			Long id = contentService.getIdByUuid(objectUUID);

			if (id == null) {
				return null;
			}
			
			Content contentCurrentVersion = contentService.getVersion(id, ContentConstants.VERSION_CURRENT);
			Content contentOriginalVersion = contentService.getVersion(id, ContentConstants.VERSION_ORIGINAL);

			AppianObjectsBean appObjBean = new AppianObjectsBean(
					Utils.getContentObjectType(contentCurrentVersion.getType(), contentCurrentVersion.getSubtype()),
					contentCurrentVersion.getId(), contentCurrentVersion.getUuid(), contentCurrentVersion.getName(),
					contentCurrentVersion.getDescription(),
					(contentCurrentVersion.getNumberOfVersions() + ConstantIfc.CURRENT_VERSION_OFFSET),
					contentCurrentVersion.getParentName(), contentCurrentVersion.getParent(), true,
					contentOriginalVersion.getCreator(), contentOriginalVersion.getCreatedTimestamp(),
					contentCurrentVersion.getCreator(), contentCurrentVersion.getUpdatedTimestamp());

			return appObjBean;
		} catch (PrivilegeException | InvalidContentException | InvalidVersionException ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}
}
