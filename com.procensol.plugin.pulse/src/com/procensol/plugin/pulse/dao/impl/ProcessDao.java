/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.NotImplementedException;

import com.appiancorp.suiteapi.common.Constants;
import com.appiancorp.suiteapi.common.ResultPage;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.process.ProcessExecutionService;
import com.appiancorp.suiteapi.process.TaskSummary;
import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.bean.SearchFilterBean;
import com.procensol.plugin.pulse.bean.TaskSummaryBean;
import com.procensol.plugin.pulse.dao.BaseDaoIfc;
import com.procensol.plugin.pulse.utils.ConstantIfc;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantDao.
 */

public class ProcessDao extends BaseDao implements BaseDaoIfc {

	/** The dao. */
	private static ProcessDao dao;

	/** The pe service. */
	private ProcessExecutionService peService;
	// private ProcessDesignService pdService;

	/**
	 * Instantiates a new process dao.
	 *
	 * @param peService the pe service
	 */
	ProcessDao(ProcessExecutionService peService) {
		this.peService = peService;
	}

	/**
	 * Gets the dao.
	 *
	 * @param peService the pe service
	 * @return the dao
	 */
	public static ProcessDao getDao(ProcessExecutionService peService) {
		if (dao == null) {
			dao = new ProcessDao(peService);
		}
		return dao;
	}

	/**
	 * Gets the task summary for process models.
	 *
	 * @param processModelIds the process model ids
	 * @param startIndex      the start index
	 * @param batchSize       the batch size
	 * @return the task summary for process models
	 * @throws PrivilegeException the privilege exception
	 */
	public ResultBean getTaskSummaryForProcessModels(Long[] processModelIds, int startIndex, int batchSize)
			throws PrivilegeException {
		return getTaskSummaryForProcessModels(processModelIds, startIndex, batchSize,
				TaskSummary.TASK_STATUS_PAUSED_BY_EXCEPTION);
	}

	/**
	 * Gets the task summary for process models.
	 *
	 * @param processModelIds the process model ids
	 * @param filters         the filters
	 * @param aggregate       the aggregate
	 * @return the task summary for process models
	 * @throws PrivilegeException the privilege exception
	 */
	public ResultBean getTaskSummaryForProcessModels(Long[] processModelIds, String filters[], boolean aggregate)
			throws PrivilegeException {
		return getTaskSummaryForProcessModels(processModelIds, filters, aggregate,
				TaskSummary.TASK_STATUS_PAUSED_BY_EXCEPTION);
	}

	/**
	 * Gets the task summary for process models.
	 *
	 * @param processModelIds the process model ids
	 * @param startIndex      the start index
	 * @param batchSize       the batch size
	 * @param taskStatus      the task status
	 * @return the task summary for process models
	 * @throws PrivilegeException the privilege exception
	 */
	public ResultBean getTaskSummaryForProcessModels(Long[] processModelIds, int startIndex, int batchSize,
			int taskStatus) throws PrivilegeException {

		List<TaskSummaryBean> taskSummaryObjectsList = new ArrayList<>();

		ResultPage resultPage = peService.getTasksWithStatusForProcessModels(processModelIds, taskStatus, true,
				startIndex, batchSize);

		long totalCount = resultPage.getAvailableItems();

		Object[] taskSummaryObjsArr = resultPage.getResults();

		for (Object tsObj : taskSummaryObjsArr) {
			TaskSummaryBean tempTSBean = new TaskSummaryBean((TaskSummary) tsObj);
			taskSummaryObjectsList.add(tempTSBean);
		}
		return new ResultBean(totalCount, taskSummaryObjectsList);
	}

	/**
	 * Gets the task summary for process models.
	 *
	 * @param processModelIds the process model ids
	 * @param filters         the filters
	 * @param aggregate       the aggregate
	 * @param taskStatus      the task status
	 * @return the task summary for process models
	 * @throws PrivilegeException the privilege exception
	 */
	public ResultBean getTaskSummaryForProcessModels(Long[] processModelIds, String filters[], boolean aggregate,
			int taskStatus) throws PrivilegeException {

		List<TaskSummaryBean> taskSummaryObjectsList = new ArrayList<>();

		ResultPage resultPage = peService.getTasksWithStatusForProcessModels(processModelIds, taskStatus, true,
				ConstantIfc.MIN_START_INDEX, Constants.COUNT_ALL);

		Object[] taskSummaryObjsArr = resultPage.getResults();

		for (Object tsObj : taskSummaryObjsArr) {
			taskSummaryObjectsList.add(new TaskSummaryBean((TaskSummary) tsObj));
		}

		List<TaskSummaryBean> uniqueTaskSummaryObjectsList = (aggregate)
				? Utils.removeDuplicates(taskSummaryObjectsList)
				: taskSummaryObjectsList;

		List<TaskSummaryBean> filteredTaskSummaryObjectsList = new ArrayList<>();

		for (int index = 0; index < filters.length; index++) {
			SearchFilterBean sfBean = new SearchFilterBean();
			sfBean.buildMe(filters[index]);
			if (index < 1) {
				filteredTaskSummaryObjectsList = filterTaskSummary(uniqueTaskSummaryObjectsList, sfBean);
			} else {
				filteredTaskSummaryObjectsList = filterTaskSummary(filteredTaskSummaryObjectsList, sfBean);
			}
		}

		return new ResultBean(filteredTaskSummaryObjectsList.size(), filteredTaskSummaryObjectsList);
	}

	/**
	 * Gets the object.
	 *
	 * @param uuid the uuid
	 * @return the object
	 * @throws RuntimeException the runtime exception
	 */
	@Override
	public AppianObjectsBean getObject(Object uuid) throws RuntimeException {
		throw new NotImplementedException();
	}

	/**
	 * Filter task summary.
	 *
	 * @param list   the list
	 * @param sfBean the sf bean
	 * @return the list
	 */
	public List<TaskSummaryBean> filterTaskSummary(List<TaskSummaryBean> list, SearchFilterBean sfBean) {

		if (sfBean.getFieldName().equalsIgnoreCase(TaskSummaryBean.PROCESS_NAME)) {
			return filterByProcessName(list, sfBean);
		} else if (sfBean.getFieldName().equalsIgnoreCase(TaskSummaryBean.TASK_NAME)) {
			return filterByTaskName(list, sfBean);
		} else if (sfBean.getFieldName().equalsIgnoreCase(TaskSummaryBean.STATUS_TIME)) {
			return filterByStatusTime(list, sfBean);
		}

		return null;

	}

	/**
	 * Filter by task name.
	 *
	 * @param list   the list
	 * @param sfBean the sf bean
	 * @return the list
	 */
	private List<TaskSummaryBean> filterByTaskName(List<TaskSummaryBean> list, SearchFilterBean sfBean) {

		CollectionUtils.filter(list, new Predicate() {

			@Override
			public boolean evaluate(Object o) {
				if (sfBean.getOperation().equalsIgnoreCase(ConstantIfc.OP_IS)) {
					return ((TaskSummaryBean) o).getTaskName().equalsIgnoreCase(sfBean.getOprands()[0]);
				} else if (sfBean.getOperation().equalsIgnoreCase(ConstantIfc.OP_LIKE)) {
					return ((TaskSummaryBean) o).getTaskName().contains(sfBean.getOprands()[0]);
				} else {
					return false;
				}
			}

		});
		return list;
	}

	/**
	 * Filter by process name.
	 *
	 * @param list   the list
	 * @param sfBean the sf bean
	 * @return the list
	 */
	private List<TaskSummaryBean> filterByProcessName(List<TaskSummaryBean> list, SearchFilterBean sfBean) {

		CollectionUtils.filter(list, new Predicate() {

			@Override
			public boolean evaluate(Object o) {

				if (sfBean.getOperation().equalsIgnoreCase(ConstantIfc.OP_IS)) {
					return ((TaskSummaryBean) o).getProcessName().equalsIgnoreCase(sfBean.getOprands()[0]);
				} else if (sfBean.getOperation().equalsIgnoreCase(ConstantIfc.OP_LIKE)) {
					return ((TaskSummaryBean) o).getProcessName().contains(sfBean.getOprands()[0]);
				} else {
					return false;
				}
			}

		});
		return list;
	}

	/**
	 * Filter by status time.
	 *
	 * @param list   the list
	 * @param sfBean the sf bean
	 * @return the list
	 */
	private List<TaskSummaryBean> filterByStatusTime(List<TaskSummaryBean> list, SearchFilterBean sfBean) {

		CollectionUtils.filter(list, new Predicate() {

			@Override
			public boolean evaluate(Object o) {

				try {
					return Utils.compareDate(((TaskSummaryBean) o).getStatusTime(), sfBean.getOprands(),
							sfBean.getOperation());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return false;
			}

		});
		return list;
	}

}
