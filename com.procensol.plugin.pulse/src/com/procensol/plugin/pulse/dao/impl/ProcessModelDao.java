/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Arrays;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.common.Constants;
import com.appiancorp.suiteapi.common.ResultPage;
import com.appiancorp.suiteapi.common.exceptions.InvalidProcessModelException;
import com.appiancorp.suiteapi.common.exceptions.PrivilegeException;
import com.appiancorp.suiteapi.process.ProcessDesignService;
import com.appiancorp.suiteapi.process.ProcessModel;
import com.appiancorp.suiteapi.process.ProcessModelFolder;
import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.dao.BaseDaoIfc;
import com.procensol.plugin.pulse.utils.ConstantIfc;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantDao.
 */

public class ProcessModelDao extends BaseDao implements BaseDaoIfc {

	/** The dao. */
	private static ProcessModelDao dao;

	/** The pd service. */
	private ProcessDesignService pdService;

	/**
	 * Instantiates a new process model dao.
	 *
	 * @param pdService the pd service
	 */
	ProcessModelDao(ProcessDesignService pdService) {
		this.pdService = pdService;
	}

	/**
	 * Gets the dao.
	 *
	 * @param pdService the pd service
	 * @return the dao
	 */
	public static ProcessModelDao getDao(ProcessDesignService pdService) {
		if (dao == null) {
			dao = new ProcessModelDao(pdService);
		}
		return dao;
	}

	/**
	 * Gets the object.
	 *
	 * @param uuid the uuid
	 * @return the object
	 * @throws RuntimeException the runtime exception
	 */
	@Override
	public AppianObjectsBean getObject(Object uuid) throws RuntimeException {
		try {
			
			ProcessModel processModel = pdService.getProcessModelByUuid(String.valueOf(uuid));
			ProcessModelFolder processFolder = pdService.getFolder(processModel.getFolderId());
			
			AppianObjectsBean appObjBean = new AppianObjectsBean(ConstantIfc.PROCESS_MODEL_KEY, processModel.getId(),
					processModel.getUuid(), Utils.formatStringWithLocal(processModel.getName().toString()),
					Utils.formatStringWithLocal(processModel.getDescription().toString()),
					getNumberOfVersionsForModel(processModel.getId()),
					Utils.formatModelLocation(processFolder.getAncestorFolderNames(), processFolder.getName()),
					processFolder.getId(), processModel.getIsPublic(), processModel.getLastModifiedUsername(),
					processModel.getTimeStampUpdated(), processModel.getCreatorUsername(),
					processModel.getTimeStampCreated());

			return appObjBean;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}

	}

	/**
	 * Format time stamp.
	 *
	 * @param tStamp the t stamp
	 * @param sc the sc
	 * @return the string
	 */
	private String formatTimeStamp(Timestamp tStamp, ServiceContext sc) {
		return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, sc.getLocale()).format(tStamp);
	}

	/**
	 * Gets the number of versions for model.
	 *
	 * @param processModelId the process model id
	 * @return the number of versions for model
	 * @throws PrivilegeException the privilege exception
	 * @throws InvalidProcessModelException the invalid process model exception
	 */
	private long getNumberOfVersionsForModel(Long processModelId)
			throws PrivilegeException, InvalidProcessModelException {
		ResultPage rp;
		rp = pdService.getVersionsForProcessModel(processModelId, 0, 1, ProcessModel.Descriptor.SORT_PM_VERSION,
				Constants.SORT_ORDER_DESCENDING);
		return rp.getAvailableItems();
	}

}
