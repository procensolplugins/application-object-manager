/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.dao.impl;

import java.util.Date;

import javax.xml.namespace.QName;

import com.appiancorp.suiteapi.type.DatatypeProperties;
import com.appiancorp.suiteapi.type.TypeService;
import com.appiancorp.type.DataType;
import com.procensol.plugin.pulse.bean.AppianObjectsBean;
import com.procensol.plugin.pulse.dao.BaseDaoIfc;
import com.procensol.plugin.pulse.utils.ConstantIfc;

// TODO: Auto-generated Javadoc
/**
 * The Class ConstantDao.
 */

public class TypeDao extends BaseDao implements BaseDaoIfc {

	/** The dao. */
	private static TypeDao dao;

	/** The type service. */
	private TypeService typeService;

	/**
	 * Instantiates a new type dao.
	 *
	 * @param typeService the type service
	 */
	TypeDao(TypeService typeService) {
		this.typeService = typeService;
	}

	/**
	 * Gets the dao.
	 *
	 * @param typeService the type service
	 * @return the dao
	 */
	public static TypeDao getDao(TypeService typeService) {
		if (dao == null) {
			dao = new TypeDao(typeService);
		}
		return dao;
	}

	/**
	 * Gets the object.
	 *
	 * @param qName the q name
	 * @return the object
	 * @throws RuntimeException the runtime exception
	 */
	@Override
	public AppianObjectsBean getObject(Object qName) throws RuntimeException {
		try {

			DataType dtType = typeService.getTypeByQualifiedName((QName)qName);
			DatatypeProperties dtProp = typeService.getDatatypeProperties(dtType.getId());

			AppianObjectsBean appObjBean = new AppianObjectsBean(ConstantIfc.DATATYPE_KEY, dtType.getId(),
					qName.toString(), dtType.getName(), dtType.getDescription(),
					Long.valueOf(dtProp.getVersions().length), ConstantIfc.NA, 0L, !dtType.isHidden(),
					/* yet to fix modified on, modified by */
					dtProp.getCreator(), new Date(dtProp.getCreationTime().getTime()), dtProp.getCreator(),
					new Date(dtProp.getCreationTime().getTime()));

			return appObjBean;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}

	}

}
