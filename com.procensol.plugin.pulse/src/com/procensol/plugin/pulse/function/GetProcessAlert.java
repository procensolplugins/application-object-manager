/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.function;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;
import com.appiancorp.suiteapi.process.ProcessExecutionService;
import com.appiancorp.suiteapi.process.analytics2.ProcessAnalyticsService;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.manager.ProcessManager;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class EvaluateBigNumber.
 */
@Category("category.name.pulse")
public class GetProcessAlert {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(GetProcessAlert.class);

	/**
	 * Gets the process alerts.
	 *
	 * @param sc
	 *            the sc
	 * @param peService
	 *            the pe service
	 * @param paService
	 *            the pa service
	 * @param contentService
	 *            the content service
	 * @param startIndex
	 *            the start index
	 * @param batchSize
	 *            the batch size
	 * @return the process alerts
	 */
	@Function
	public String getProcessAlerts(ServiceContext sc, ProcessExecutionService peService,
			ProcessAnalyticsService paService, ContentService contentService,
			@Parameter(required = true) int startIndex, @Parameter(required = true) int batchSize) {

		try {
			ProcessManager manager = ProcessManager.getManager(peService, paService, contentService);
			LOG.info("Manager initialized" + manager);
			return manager.getProcessAlerts(startIndex, batchSize);
		} catch (Exception e) {
			LOG.debug(e);
			return Utils.convertToJson(new ResultBean(true, e.getMessage()));
		}

	}

}
