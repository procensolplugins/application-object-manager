/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.function;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;
import com.appiancorp.suiteapi.process.ProcessExecutionService;
import com.appiancorp.suiteapi.process.analytics2.ProcessAnalyticsService;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.manager.ProcessManager;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class EvaluateBigNumber.
 */
@Category("category.name.pulse")
public class GetProcessAlertsByFilters {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(GetProcessAlertsByFilters.class);

	/**
	 * Gets the process alerts.
	 *
	 * @param sc            the sc
	 * @param peService            the pe service
	 * @param paService            the pa service
	 * @param contentService            the content service
	 * @param searchFilters the search filters
	 * @param aggregate the aggregate
	 * @return the process alerts
	 */
	@Function
	public String getProcessAlertsByFilters(ServiceContext sc, ProcessExecutionService peService,
			ProcessAnalyticsService paService, ContentService contentService,
			@Parameter(required = true) String[] searchFilters,
			@Parameter(required = true) boolean aggregate) {

		try {
			ProcessManager manager = ProcessManager.getManager(peService, paService, contentService);
			LOG.info("Manager initialized" + manager);
			
			return manager.getProcessAlertsByFilters(searchFilters, aggregate);
		} catch (Exception e) {
			LOG.debug(e);
			return Utils.convertToJson(new ResultBean(true, e.getMessage()));
		}

	}

}
