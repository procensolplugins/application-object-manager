/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.function;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.applications.ApplicationService;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.expression.annotations.Category;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;
import com.appiancorp.suiteapi.process.ProcessDesignService;
import com.appiancorp.suiteapi.type.TypeService;
import com.procensol.plugin.pulse.bean.ResultBean;
import com.procensol.plugin.pulse.manager.ApplicationManager;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class EvaluateBigNumber.
 */
@Category("category.name.pulse")
public class CollisionTracker {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CollisionTracker.class);

	/**
	 * Gets the process alerts.
	 *
	 * @param sc            the sc
	 * @param appService the app service
	 * @param contentService            the content service
	 * @param pdService the pd service
	 * @param typeService the type service
	 * @param applicationUUIDsOrIds the application UUI ds or ids
	 * @return the process alerts
	 */
	@Function
	public String getCollisions(ServiceContext sc, 
			ApplicationService appService, 
			ContentService contentService, 
			ProcessDesignService pdService,
			TypeService typeService,
			@Parameter(required = true) String [] applicationUUIDsOrIds) {

		try {
			
			ApplicationManager manager = ApplicationManager.getManager(appService, contentService, pdService, typeService);
			LOG.info("Manager initialized" + manager);
			return manager.getCollisions(applicationUUIDsOrIds);
		} catch (Exception e) {
			LOG.debug(e);
			return Utils.convertToJson(new ResultBean(true, e.getMessage()));
		}

	}

}
