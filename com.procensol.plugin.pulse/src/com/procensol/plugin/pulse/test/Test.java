/*
 * @author Ritesh Pareek
 * @company Procensol
 */
package com.procensol.plugin.pulse.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.procensol.plugin.pulse.bean.TaskSummaryBean;
import com.procensol.plugin.pulse.dao.impl.ProcessDao;
import com.procensol.plugin.pulse.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Test.
 */
public class Test {

	/**
	 * The main method.
	 *
	 * @param as
	 *            the arguments
	 */
	public static void main(String[] as) {
		
		String [] filters = new String[] {"fieldName:processName;operation:LIKE;oprands:PN"};

		List<TaskSummaryBean> taskSummaryObjectsList = new ArrayList<>();

		TaskSummaryBean t1 = new TaskSummaryBean();
		t1.setProcessName("PN1");
		t1.setTaskName("TN1");
		taskSummaryObjectsList.add(t1);
		
		t1 = new TaskSummaryBean();
		t1.setProcessName("PN2");
		t1.setTaskName("TN2");
		taskSummaryObjectsList.add(t1);
		
		t1 = new TaskSummaryBean();
		t1.setProcessName("PN");
		t1.setTaskName("TN");
		taskSummaryObjectsList.add(t1);
		
		t1 = new TaskSummaryBean();
		t1.setProcessName("PN");
		t1.setTaskName("TN");
		taskSummaryObjectsList.add(t1);

//		List<TaskSummaryBean> filteredTaskSummaryObjectsList = new ArrayList<>();

		ProcessDao processDao = ProcessDao.getDao(null);
		String[] names = "{en_GB=TestChild English, en_US=, fr_FR=Test Child FR}".replaceAll("\\{", "").replaceAll("\\}", "").split(",");
		Map<String, String> namesMap= new HashMap<String, String>();
		for (String name : names) {
			String []lang = name.split("=");
			namesMap.put(lang[0].trim(), lang.length > 1 ?  lang[1] : "NA");
			System.out.println(Utils.convertToJson(namesMap));
		}
		System.out.println(Utils.convertToJson("[en_GB=TestParent]"));
		
		//ResultBean rs = processDao.deletmenAsap(taskSummaryObjectsList, true, filters);
		
		//System.out.println(Utils.convertToJson(rs));
				
//		for (String filter : filters) {
//			SearchFilterBean sfBean = new SearchFilterBean();
//			sfBean.buildMe(filter);
//			System.out.println("Search Filter Bean built : " + sfBean);
//			List<TaskSummaryBean> temp = new ArrayList<>();
//			;
//			System.out.println(temp);
//			filteredTaskSummaryObjectsList.addAll(temp);
//			System.out.println(sfBean.toString());
//			System.out.println("After Filtering :: " + filteredTaskSummaryObjectsList.size());
//		}
	}
}
